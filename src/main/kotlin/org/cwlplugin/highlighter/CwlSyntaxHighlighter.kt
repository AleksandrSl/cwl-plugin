package org.cwlplugin.highlighter

import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.HighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.tree.IElementType
import org.cwlplugin.lexer.CwlHighlightingLexer
import org.cwlplugin.parser.CwlTokenTypes


class CwlSyntaxHighlighter : SyntaxHighlighterBase() {

    companion object {

        val REQUIREMENTS: TextAttributesKey =
                createTextAttributesKey("CWL_REQUIREMENTS", DefaultLanguageHighlighterColors.CONSTANT)
        val INPUTS: TextAttributesKey =
                createTextAttributesKey("CWL_INPUTS", DefaultLanguageHighlighterColors.LABEL)
        val OUTPUTS: TextAttributesKey =
                createTextAttributesKey("CWL_OUTPUTS", DefaultLanguageHighlighterColors.LABEL)
        val DOC: TextAttributesKey =
                createTextAttributesKey("CWL_DOCS", DefaultLanguageHighlighterColors.DOC_COMMENT)
        val SEPARATOR: TextAttributesKey =
                createTextAttributesKey("CWL_SEPARATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN)
        val STRING: TextAttributesKey =
                createTextAttributesKey("CWL_VALUE", DefaultLanguageHighlighterColors.STRING)
        val COMMENT: TextAttributesKey =
                createTextAttributesKey("CWL_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT)
        val BAD_CHARACTER: TextAttributesKey =
                createTextAttributesKey("CWL_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER)
        val STEPS: TextAttributesKey =
                createTextAttributesKey("CWL_STEPS", DefaultLanguageHighlighterColors.LABEL)
        val MLS: TextAttributesKey =
                createTextAttributesKey("CWL_MULTI_LINE_STRING", DefaultLanguageHighlighterColors.BLOCK_COMMENT)
        val TYPE: TextAttributesKey =
                createTextAttributesKey("CWL_TYPE", DefaultLanguageHighlighterColors.CONSTANT)

        private val REQUIREMENTS_KEYS: Array<TextAttributesKey> = arrayOf(REQUIREMENTS)
        private val INPUTS_KEYS: Array<TextAttributesKey> = arrayOf(INPUTS)
        private val OUTPUTS_KEYS: Array<TextAttributesKey> = arrayOf(OUTPUTS)
        private val DOC_KEYS: Array<TextAttributesKey> = arrayOf(DOC)
        private val BAD_CHAR_KEYS: Array<TextAttributesKey> = arrayOf(BAD_CHARACTER)
        private val SEPARATOR_KEYS: Array<TextAttributesKey> = arrayOf(SEPARATOR)
        private val COMMENT_KEYS: Array<TextAttributesKey> = arrayOf(COMMENT)
        private val STEPS_KEYS = arrayOf(STEPS)
        private val STRING_KEYS = arrayOf(STRING)
        private val MLS_KEYS = arrayOf(MLS)
        private val TYPE_KEYS = arrayOf(TYPE)
        private val EMPTY_KEYS: Array<TextAttributesKey> = arrayOf()
    }

    override fun getHighlightingLexer(): Lexer = CwlHighlightingLexer()

    override fun getTokenHighlights(tokenType: IElementType?): Array<TextAttributesKey> {

        with(CwlTokenTypes) {
            return when (tokenType) {

                com.intellij.psi.TokenType.BAD_CHARACTER -> BAD_CHAR_KEYS
                REQUIREMENTS_KEYWORD -> REQUIREMENTS_KEYS
                INPUTS_KEYWORD -> INPUTS_KEYS
                OUTPUTS_KEYWORD -> OUTPUTS_KEYS
                DOC_KEYWORD -> DOC_KEYS
                END_OF_LINE_COMMENT -> COMMENT_KEYS
                COLON -> SEPARATOR_KEYS
                STRING -> STRING_KEYS
                STEPS_KEYWORD -> STEPS_KEYS
                MLSPART -> MLS_KEYS
                else -> {
                    return if (tokenType in CWL_TYPES) TYPE_KEYS else EMPTY_KEYS
                }
            }
        }
    }
}