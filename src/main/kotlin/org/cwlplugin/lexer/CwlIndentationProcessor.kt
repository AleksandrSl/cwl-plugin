package org.cwlplugin.lexer

import com.intellij.psi.tree.IElementType
import gnu.trove.TIntStack
import org.cwlplugin.parser.CwlTokenTypes
import java.util.*


/**
 * Created by aleksandrsl on 17.06.17.
 */
open class CwlIndentationProcessor : CwlLexerAdapter() {

    protected val myIndentStack = TIntStack()
    protected var myBraceLevel: Int = 0
    protected var myLineHasSignificantTokens: Boolean = false
    protected var myLastNewLineIndent = -1
    private var myCurrentNewLineIndent = 0
    private var hasUnclosedColon = false
    private var hasUnclosedBracket = false
    private val RECOVERY_TOKENS = CwlTokenTypes.REQUIREMENTS_TOKENS
    protected var afterSequenceElementPrefix = false
//    private val RECOVERY_TOKENS = PythonDialectsTokenSetProvider.INSTANCE.getUnbalancedBracesRecoveryTokens()

    protected open class PendingToken(var type: IElementType?, val start: Int, val end: Int) {

        override fun toString(): String = "${type.toString()}:$start-$end"
    }

    private class PendingCommentToken(type: IElementType?, start: Int, end: Int, val indent: Int) : PendingToken(type, start, end)

    protected var myTokenQueue: MutableList<PendingToken> = ArrayList()

    protected var myProcessSpecialTokensPending = false

    protected val baseTokenType: IElementType?
        get() = super.getTokenType()

    protected val baseTokenStart: Int
        get() = super.getTokenStart()

    protected val baseTokenEnd: Int
        get() = super.getTokenEnd()

    private fun isBaseAt(tokenType: IElementType): Boolean = baseTokenType === tokenType

    var multiLineStringStarted: Boolean = false

    override fun getTokenType(): IElementType? {
        if (myTokenQueue.size > 0) {
            return myTokenQueue[0].type
        }
        return super.getTokenType()
    }

    override fun getTokenStart(): Int = if (myTokenQueue.size > 0) myTokenQueue[0].start else baseTokenStart


    override fun getTokenEnd(): Int = if (myTokenQueue.size > 0) myTokenQueue[0].end else baseTokenEnd

    override fun advance() {
        // Placed at the beginning not to miss first token
        if (DUMP_TOKENS) {
            tokenType?.let {
                println("$tokenStart-$tokenEnd:$tokenType")
                if (tokenType === CwlTokenTypes.LINE_BREAK) println("{$myBraceLevel}") else print(" ")
            }
        }
        when (tokenType) {
            CwlTokenTypes.LINE_BREAK -> {
                val text = tokenText
                var spaces = 0
                for (i in text.length - 1 downTo 0) {
                    when (text[i]) {
                        ' ' -> spaces++
                        '\t' -> spaces += 8
                    }
                }
                myCurrentNewLineIndent = spaces
            }
            CwlTokenTypes.TAB -> myCurrentNewLineIndent += 8
        }
        if (myTokenQueue.size > 0) {
            myTokenQueue.removeAt(0)
            if (baseTokenType == CwlTokenTypes.PIPE) {  // Remove this
                processSpecialTokens()
            }
        } else {
            advanceBase()
            processSpecialTokens()
        }
        checkBrackets()
    }

    protected fun advanceBase(): Unit {
        super.advance()
        checkSignificantTokens()
    }

    protected fun pushToken(type: IElementType, start: Int, end: Int): Unit {
        myTokenQueue.add(PendingToken(type, start, end))
    }

    override fun start(buffer: CharSequence, startOffset: Int, endOffset: Int, initialState: Int): Unit {
        checkStartState(startOffset, initialState)
        super.start(buffer, startOffset, endOffset, initialState)
        setStartState()
    }

    protected fun checkStartState(startOffset: Int, initialState: Int): Unit {
        if (DUMP_TOKENS) println("\n--- LEXER START---")
    }

    private fun setStartState(): Unit {
        myIndentStack.clear()
        myIndentStack.push(0)
        myBraceLevel = 0
        myLineHasSignificantTokens = false
        hasUnclosedColon()
        checkSignificantTokens()
        if (isBaseAt(CwlTokenTypes.SPACE)) {
            processIndent(0, CwlTokenTypes.SPACE, isCurrentLineSignificant = true)
        }
    }

    fun checkBrackets() {
        when (tokenType) {
            CwlTokenTypes.LBRACKET -> hasUnclosedBracket = true
            CwlTokenTypes.RBRACKET -> hasUnclosedBracket = false
        }
    }

    protected fun checkSignificantTokens() {
        /*
        Without conditional for null, last line always generates line break
         */
        myLineHasSignificantTokens = myLineHasSignificantTokens
                || (!CwlTokenTypes.NON_SIGNIFICANT_ELEMENTS.contains(baseTokenType) && baseTokenType != null)
    }

    protected open fun processSpecialTokens() {
        val tokenStart = baseTokenStart
        when {
            isBaseAt(CwlTokenTypes.LINE_BREAK) -> {
                processLineBreak(tokenStart)
            }
            isBaseAt(CwlTokenTypes.SPACE) -> {
                processSpace()
            }
            isBaseAt(CwlTokenTypes.PIPE) -> {
                processMultiLineString()
            }
        }
    }

    private fun processSpace() {
        val start = baseTokenStart
        var end = baseTokenEnd
        while (baseTokenType === CwlTokenTypes.SPACE) {
            end = baseTokenEnd
            advanceBase()
        }
        when (baseTokenType) {
            CwlTokenTypes.LINE_BREAK -> processLineBreak(start)

            CwlTokenTypes.PIPE -> processMultiLineString()

            else -> myTokenQueue.add(PendingToken(CwlTokenTypes.SPACE, start, end))
        }
    }

    /**
     * Process multi line string
     *
     * Skip all indents inside it. Multi line string ends with dedent.
     * Unfortunately it seems that I cannot replace existent token, only add new tokens.
     * So I still should parse multiline string
     */
    fun processMultiLineString(): Unit {
        var start = tokenStart
        var end = tokenEnd
        // Skip all tokens before line break. Parser will take care that they are only whitespaces
        while (baseTokenType != CwlTokenTypes.LINE_BREAK && baseTokenType != null) {
            advanceBase()
        }
        // Push token back after next is passed, to prevent infinite loops
        pushToken(CwlTokenTypes.PIPE, start, end)
        // Process first line break
        processLineBreak(baseTokenStart)  // This pushes at least one token into myTokenQueue
//        println("Initial token queue: $myTokenQueue")
        multiLineStringStarted = true
//        println("Current token after LB search block: $baseTokenType")
        start = baseTokenStart
        /*
        In sequence additional indent becomes last token, not dedent. Therefore check all queue for dedents
         */
        while (!myTokenQueue.any { it.type == CwlTokenTypes.DEDENT } && baseTokenType != null) {
            println("Current token in mls parsing while block: $baseTokenType and queue token ${myTokenQueue.last().type}")
            when (baseTokenType) {
                CwlTokenTypes.LINE_BREAK -> {
//                    println("Token queue: $myTokenQueue")
                    end = baseTokenStart
//                    println("Line break in mls")
                    pushToken(CwlTokenTypes.MLSPART, start, end)
                    // MLSPART will not be checked but it's significant
                    myLineHasSignificantTokens = true
                    processLineBreak(baseTokenStart)
                    start = myTokenQueue.last().end
//                    println("Last token type: ${myTokenQueue.last().type}")
                }
                else -> {
//                    start = baseTokenEnd
                    advanceBase()
                    /*
                    When end of the file is encountered inside mls push current mls token and line break.
                    Otherwise these tokens are missed totally
                     */
                    if (baseTokenType == null) {
                        pushToken(CwlTokenTypes.MLSPART, start, bufferEnd)
                        pushToken(CwlTokenTypes.LINE_BREAK, bufferEnd, bufferEnd)
                    }
                }
            }
        }
        multiLineStringStarted = false
    }

    protected fun hasUnclosedColon() {
        if (!hasUnclosedColon) {
            hasUnclosedColon = baseTokenType == CwlTokenTypes.COLON
        } else {
            if (!CwlTokenTypes.WHITESPACE_OR_LINEBREAK.contains(baseTokenType) && (baseTokenType != commentTokenType)) {
                hasUnclosedColon = false
            }
        }
    }

    protected fun processLineBreak(startPos: Int): Unit {
        when {
            myBraceLevel == 0 -> {
                hasUnclosedColon = false
                /*
                 Save current line significance before line break and then set it to false
                 for new string
                  */
                val isCurrentLineSignificant = myLineHasSignificantTokens
                myLineHasSignificantTokens = false
                advanceBase()
                processIndent(startPos, CwlTokenTypes.LINE_BREAK, isCurrentLineSignificant)
            }
            else -> {
                processInsignificantLineBreak(startPos, false)
            }
        }
    }

    protected fun processInsignificantLineBreak(startPos: Int,
                                                breakStatementOnLineBreak: Boolean): Unit {
        // merge whitespace following the line break character into the
        // line break token
        var end = baseTokenEnd
        advanceBase()
        with(CwlTokenTypes) {
            while (baseTokenType === SPACE || baseTokenType === TAB ||
                    !breakStatementOnLineBreak && baseTokenType === LINE_BREAK) {
                end = baseTokenEnd
                advanceBase()
            }
            myTokenQueue.add(PendingToken(LINE_BREAK, startPos, end))
            myProcessSpecialTokensPending = true
        }
    }

    protected fun processIndent(whiteSpaceStart: Int, whitespaceTokenType: IElementType,
                                isCurrentLineSignificant: Boolean): Unit {
        var lastIndent = myIndentStack.peek()
        var indent = nextLineIndent
        myLastNewLineIndent = indent
        // don't generate indent/dedent tokens if a line contains only end-of-line comment and whitespace
        if (baseTokenType === commentTokenType) {
            indent = lastIndent
        }
        // TODO change here
        val whiteSpaceEnd = if (baseTokenType == null) super.getBufferEnd() else baseTokenStart
        println("Significance in process indent $isCurrentLineSignificant")
        when {
            hasUnclosedBracket -> {
                // Just skip all end of line and indent tokens, to allow
                // multiline array literals.
            }
            indent > lastIndent -> {
                /*
                 * In multiline string only first indent after '|' matters. Following lines have the same indent
                 * and all excessive tabs and whitespaces are parts of the string
                 */
                if (multiLineStringStarted) {
                    myTokenQueue.add(PendingToken(whitespaceTokenType, whiteSpaceStart,
                            whiteSpaceEnd - indent + lastIndent)) // Stop line break on previous indent level
                } else {
                    myIndentStack.push(indent)
                    // TODO rewrite function since whiteSpace token is always line break? and it's one symbol length?
                    if (isCurrentLineSignificant) {
                        println("Indent added lb at $whiteSpaceStart-$whiteSpaceEnd")
                        myTokenQueue.add(PendingToken(whitespaceTokenType, whiteSpaceStart, whiteSpaceStart + 1))
                    }
                    var insertIndex = skipPrecedingCommentsWithIndent(indent, myTokenQueue.size - 1)
                    var indentOffset = if (insertIndex == myTokenQueue.size) whiteSpaceStart + 1 else myTokenQueue[insertIndex].start
                    myTokenQueue.add(insertIndex, PendingToken(CwlTokenTypes.INDENT, indentOffset, baseTokenStart))
                    if (afterSequenceElementPrefix) {
                        val additionalIndentStart = baseTokenEnd
                        pushCurrentToken()
                        advanceBase()
                        val additionalIndent = nextLineIndent + myIndentStack.peek() + 1
//                        println("additional indent $additionalIndent")
                        myIndentStack.push(additionalIndent)
                        insertIndex = skipPrecedingCommentsWithIndent(additionalIndent, myTokenQueue.size - 1)
                        myTokenQueue.add(insertIndex, PendingToken(CwlTokenTypes.INDENT, additionalIndentStart, baseTokenStart))
                        afterSequenceElementPrefix = false
                    }
                }
            }
            indent < lastIndent -> {
                if (isCurrentLineSignificant) {
                    println("Dedent added lb at $whiteSpaceStart-$whiteSpaceEnd")
                    myTokenQueue.add(PendingToken(whitespaceTokenType, whiteSpaceStart, whiteSpaceEnd))
                }
                while (indent < lastIndent) {
                    myIndentStack.pop()
                    lastIndent = myIndentStack.peek()
                    var insertIndex = myTokenQueue.size
                    var dedentOffset = whiteSpaceStart
                    if (indent > lastIndent) {
                        myTokenQueue.add(PendingToken(CwlTokenTypes.INCONSISTENT_DEDENT, whiteSpaceStart, whiteSpaceStart))
                        insertIndex++
                    } else {
                        insertIndex = skipPrecedingCommentsWithIndent(indent, insertIndex)
                    }
                    if (insertIndex != myTokenQueue.size) {
                        dedentOffset = myTokenQueue[insertIndex].end
                    }
                    myTokenQueue.add(insertIndex, PendingToken(CwlTokenTypes.DEDENT, dedentOffset, dedentOffset))
                }
                if (afterSequenceElementPrefix) {
                    val additionalIndentStart = baseTokenEnd
                    pushCurrentToken()
                    advanceBase()
                    val additionalIndent = nextLineIndent + myIndentStack.peek() + 1
                    myIndentStack.push(additionalIndent)
                    val insertIndex = skipPrecedingCommentsWithIndent(additionalIndent, myTokenQueue.size - 1)
                    myTokenQueue.add(insertIndex, PendingToken(CwlTokenTypes.INDENT, additionalIndentStart, baseTokenStart))
                    afterSequenceElementPrefix = false
                }
            }
            else -> {
                if (isCurrentLineSignificant) {
                    println("Else added lb at $whiteSpaceStart-$whiteSpaceEnd")
                    myTokenQueue.add(PendingToken(whitespaceTokenType, whiteSpaceStart, whiteSpaceEnd))
                }
            }
        }
    }

    protected fun skipPrecedingCommentsWithIndent(indent: Int, index: Int): Int {
        var index_copy = index
        // insert the DEDENT before previous comments that have the same indent as the current token indent
        var foundComment = false
        while (index_copy > 0 && myTokenQueue[index_copy - 1] is PendingCommentToken) {
            val commentToken = myTokenQueue[index_copy - 1] as PendingCommentToken
            if (commentToken.indent != indent) break
            foundComment = true
            index_copy--
            if (index_copy > 1 &&
                    myTokenQueue[index_copy - 1].type === CwlTokenTypes.LINE_BREAK &&
                    myTokenQueue[index_copy - 2] is PendingCommentToken) {
                index_copy--
            }
        }
        return if (foundComment) index_copy else myTokenQueue.size
    }

    protected val nextLineIndent: Int
        get() {
            var indent = 0
            while (baseTokenType != null && CwlTokenTypes.WHITESPACE_OR_LINEBREAK.contains(baseTokenType)) {
                with(CwlTokenTypes) {
                    when (baseTokenType) {
                        TAB -> indent = (indent / 8 + 1) * 8
                        SPACE -> indent++
                        LINE_BREAK -> indent = 0
                    }
                    advanceBase()
                }
            }
            afterSequenceElementPrefix = baseTokenType == CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX
            println("Sequence started")
            return if (baseTokenType == null) 0 else indent
        }

    private fun pushCurrentToken(): Unit {
        myTokenQueue.add(PendingToken(baseTokenType, baseTokenStart, baseTokenEnd))
    }

    protected val commentTokenType: IElementType
        get() = CwlTokenTypes.END_OF_LINE_COMMENT

    companion object {

        private const val DUMP_TOKENS = true
    }
}
