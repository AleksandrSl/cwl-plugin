package org.cwlplugin.lexer

import org.cwlplugin.parser.CwlTokenTypes

/**
 * @author yole
 * Modified by aleksandrsl
 */
class CwlIndentationLexer : CwlIndentationProcessor() {

    private var addFinalBreak = true
    override fun processSpecialTokens() {
        super.processSpecialTokens()
        val tokenStart = baseTokenStart
        // If myTokenQueue is not empty at the end of the file, then it already has a line break
        if (baseTokenType == null && addFinalBreak && myLineHasSignificantTokens && myTokenQueue.isEmpty()) {
            pushToken(CwlTokenTypes.LINE_BREAK, tokenStart, tokenStart)
            addFinalBreak = false
        }
    }
}
