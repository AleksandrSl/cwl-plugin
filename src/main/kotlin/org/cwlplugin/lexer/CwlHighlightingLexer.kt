package org.cwlplugin.lexer

import com.intellij.psi.tree.IElementType
import org.cwlplugin.parser.CwlTokenTypes
import java.util.ArrayList
import java.util.LinkedList

/**
 * Created by aleksandrsl on 17.06.17.
 */
class CwlHighlightingLexer : CwlLexerAdapter() {

    private val indentStack: MutableList<Int> = LinkedList()
    private var currentIndent = 0

    protected open class PendingToken(var type: IElementType?, val start: Int, val end: Int) {

        override fun toString(): String = "${type.toString()}:$start-$end"
    }

    protected var myTokenQueue: MutableList<PendingToken> = ArrayList()

    protected val baseTokenStart: Int
        get() = super.getTokenStart()

    protected val baseTokenEnd: Int
        get() = super.getTokenEnd()

    private var contentStarted: Boolean = false

    private var multiLineStringStarted: Boolean = false

    var multiLineStringIndent: Int? = null

    override fun getTokenType(): IElementType? {
        if (myTokenQueue.size > 0) {
            return myTokenQueue.removeAt(0).type
        }
        return super.getTokenType()
    }

    override fun getTokenStart(): Int = if (myTokenQueue.size > 0) myTokenQueue[0].start else baseTokenStart

    override fun getTokenEnd(): Int = if (myTokenQueue.size > 0) myTokenQueue[0].end else baseTokenEnd

    override fun advance() {
        when (tokenType) {
            CwlTokenTypes.LINE_BREAK -> {
                currentIndent = 0
                contentStarted = false
            }
            CwlTokenTypes.PIPE -> {
                multiLineStringStarted = true
                println("MLS power!!!!!")
            }
            else -> {
                println("Content started before: $contentStarted")
                if (!contentStarted) {

                    when (tokenType) {
                        CwlTokenTypes.SPACE -> {
                            currentIndent++
                        }
                        CwlTokenTypes.TAB -> currentIndent += 8
                        CwlTokenTypes.FORMFEED -> null
                        else -> {
                            contentStarted = true
                            // Add current indent to stack when meaningful part of line has started
                            processIndent(currentIndent)
                        }
                    }
                    /*
                     If it's first string in MLS activate multiline string state just after possible
                     indent, since we don't know where content will start
                      */
                    if (multiLineStringStarted && (multiLineStringIndent == null)
                            && !flex.isInMultiLineStringState
                            && currentIndent > indentStack.last()) {
                        flex.yybeginMultiLineString()
                    }
                    /*
                     If we are in MLS and it's not the first string.
                     Every line by default starts as YYINITAL state. This is done to prevent lexing of
                     first word after MLS as part of MLS.
                     e.g.
                     ```
                     doc: |
                        Formats: bed/gff/vcf
                        The input file (-i) file must be sorted by chrom, then start.
                     strandedness:
                     ```
                     without falling back to YYINITAL state, strandedness will be recognized as MLS, because
                     we don't know that MLS has ended at the time. We will know that it has ended only after first
                     token on the next line will be acquired and found that dedent took place

                     However if on the next line we encounter at least the same indent level, we know that we
                     are still in MLS and state can be switched to MULTILINE_STRING

                     P.S. To hell with strings without quotes
                     */
                    if (multiLineStringStarted && (multiLineStringIndent != null)) {
                        // TODO Think about tabs here
                        if (currentIndent == multiLineStringIndent) {
                            flex.yybeginMultiLineString()
                        }
                    }
                }
            }
        }
        println("Current indent: $currentIndent")
        println("Content started: $contentStarted")
        super.advance()
    }

    private fun processIndent(indent: Int) {
        val lastIndent = indentStack.last()
        println("Last indent: $lastIndent")
        when {
            indent > lastIndent -> {
                if (multiLineStringStarted) {
                    if (multiLineStringIndent != null) return
                    else multiLineStringIndent = indent
                }
                println("Indent")
                indentStack.add(indent)
            }
            indent < lastIndent -> {
                println("indent < lastIndent")
                if (multiLineStringStarted) {
                    println("MLS at an end!!!")
                    multiLineStringIndent = null
                    multiLineStringStarted = false
                }
                indentStack.lastIndexOf(indent)
                        .takeIf { it != -1 }
                        ?.let {
                    while (indentStack.size - 1 > it) {
                        indentStack.removeAt(indentStack.size - 1)
                        println("Dedent")
                    }
                }
            }
        }
        return
    }

    override fun start(buffer: CharSequence, startOffset: Int, endOffset: Int, initialState: Int) {
        super.start(buffer, startOffset, endOffset, initialState)
        setStartState()
    }

    private fun setStartState() {
        indentStack.clear()
        indentStack.add(0)
        multiLineStringStarted = false
        multiLineStringIndent = null
        contentStarted = false
    }
}