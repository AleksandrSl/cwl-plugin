package org.cwlplugin.codeInsight

import com.intellij.lang.BracePair
import com.intellij.lang.PairedBraceMatcher
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType
import org.cwlplugin.parser.CwlTokenTypes

class CwlBraceMatcher : PairedBraceMatcher {

    override fun getCodeConstructStart(file: PsiFile?, openingBraceOffset: Int): Int = openingBraceOffset

    override fun getPairs(): Array<BracePair> {
        return with(CwlTokenTypes) {
            arrayOf(BracePair(LBRACKET, RBRACKET, false))
        }
    }

    override fun isPairedBracesAllowedBeforeType(lbraceType: IElementType, contextType: IElementType?): Boolean = true
}