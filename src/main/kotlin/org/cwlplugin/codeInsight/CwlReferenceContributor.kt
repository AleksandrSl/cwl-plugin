package org.cwlplugin.codeInsight

import com.intellij.openapi.util.TextRange
import com.intellij.patterns.PlatformPatterns
import com.intellij.psi.*
import com.intellij.util.ProcessingContext
import org.cwlplugin.psi.CwlInputParameterStatement
import org.cwlplugin.psi.CwlStepInput
import org.cwlplugin.psi.reference.CwlInputParameterReference

class CwlReferenceContributor : PsiReferenceContributor() {

    override fun registerReferenceProviders(registrar: PsiReferenceRegistrar) {
        registrar.registerReferenceProvider(PlatformPatterns.psiElement(CwlInputParameterStatement::class.java).inside(CwlStepInput::class.java),
                object : PsiReferenceProvider() {

                    override fun getReferencesByElement(element: PsiElement, context: ProcessingContext): Array<PsiReference> {
                        val statement: CwlInputParameterStatement = element as CwlInputParameterStatement
                        return arrayOf(
                                CwlInputParameterReference(statement, TextRange(0, statement.keyText.length))
                        )
                    }
                })
    }
}
