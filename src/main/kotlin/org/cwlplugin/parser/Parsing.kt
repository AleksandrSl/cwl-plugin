package org.cwlplugin.parser

import com.intellij.lang.PsiBuilder
import com.intellij.openapi.diagnostic.Logger
import com.intellij.psi.tree.IElementType
import org.cwlplugin.CwlBundle
import org.cwlplugin.psi.CwlElementType
import org.cwlplugin.psi.CwlElementTypes

/**
 * @author yole, Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
open class Parsing(val parsingContext: ParsingContext) {

    private val LOG = Logger.getInstance(Parsing::class.java.name)

    protected val myBuilder: PsiBuilder = parsingContext.builder

    protected fun checkMatches(token: IElementType
                               , message: String? = null
                               , advanceLexer: Boolean = true
    ): Boolean {

        return if (myBuilder.tokenType === token) {  // == is used instead of equals in python source
            if (advanceLexer) myBuilder.advanceLexer()
            true
        } else {
            message?.let { myBuilder.error(message) }
            false
        }
    }

    protected fun assertCurrentToken(tokenType: CwlElementType
    ) {
        LOG.assertTrue(myBuilder.tokenType === tokenType)
    }

    protected fun atToken(tokenType: IElementType
    ): Boolean = myBuilder.tokenType === tokenType

    protected fun atToken(tokenType: IElementType
                          , tokenText: String
    ): Boolean =
            myBuilder.tokenType === tokenType && tokenText == myBuilder.tokenText

    protected fun atAnyOfTokens(vararg tokenTypes: IElementType
    ): Boolean =
            tokenTypes.any { myBuilder.tokenType === it }

    protected fun matchToken(tokenType: IElementType
    ): Boolean =
            if (myBuilder.tokenType === tokenType) {
                myBuilder.advanceLexer()
                true
            } else false

    protected fun nextToken() {
        myBuilder.advanceLexer()
    }

    protected fun buildTokenElement(type: IElementType
                                    , builder: PsiBuilder
    ) {
        val marker = builder.mark()
        builder.advanceLexer()
        marker.done(type)
    }

    protected fun reportParseStatementError(builder: PsiBuilder
                                            , firstToken: IElementType
    ) {
        when (firstToken) {
            CwlTokenTypes.INCONSISTENT_DEDENT -> {
                builder.error("Unindent does not match any outer indentation level")
            }
            CwlTokenTypes.INDENT -> {
                builder.error("Unexpected indent")
            }
            else -> {
                builder.error("Statement expected, found " + firstToken.toString())
            }
        }
    }

    /**
     *
     */
    protected inline fun parseSequence(blockType: IElementType = CwlElementTypes.SEQUENCE
                                       , parseElement: () -> Boolean
    ): Boolean {

        parsingContext.pushScope(parsingContext.currentScope.withSequence())
        if (checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))) {
            println("Instead indent: ${myBuilder.tokenType}")
            if (checkMatches(CwlTokenTypes.INDENT, "Indent expected", advanceLexer = false)) {
                val indentedBlock: PsiBuilder.Marker = myBuilder.mark()
                nextToken()
                if (myBuilder.eof()) {
                    myBuilder.error("Indented block expected")
                } else {
                    checkMatches(CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX, CwlBundle.message("PARSE.expected.sequence_element_prefix"))
                    parseSingleIndentedStatement {
                        parseEither(parseFirstElement = { parseSaladDirective() }
                                , parseSecondElement = { parseElement() })
                    }
                    while (!myBuilder.eof() && !atToken(CwlTokenTypes.DEDENT)) {
                        print("Parse Sequence: At token ${myBuilder.tokenType}")
                        if (!checkMatches(CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX, CwlBundle.message("PARSE.expected.sequence_element_prefix_or_dedent"))) {
                            break
                        }
                        if (!parseSingleIndentedStatement {
                            parseEither(parseFirstElement = { parseSaladDirective() }
                                    , parseSecondElement = { parseElement() })
                        }) nextToken() // Skip tokens until dedent if nothing matches
                    }
                    print("escaped!")
                }
                if (!myBuilder.eof()) {
                    checkMatches(CwlTokenTypes.DEDENT, "Dedent expected")
                }
                indentedBlock.done(blockType)
            }
        }
        parsingContext.popScope()
        // TODO make Unit?
        return true
    }

    protected fun parseSimpleSequence(blockType: IElementType
                                      , elementType: CwlElementType
    ): Boolean {
        return parseSequence(blockType = blockType) {
            matchToken(elementType)
            checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
        }
    }

    /**
     * Parse full statements like "key: value"
     *
     * @param parseKey lambda that parses key
     * @param parseValue lambda that parses value
     * @param statementElement element that will be build around statement
     *
     * @return true if statement is correct false otherwise
     */
    protected inline fun checkFullStatement(statementElement: IElementType = CwlElementTypes.STATEMENT
                                            , parseKey: () -> Boolean
                                            , parseValue: () -> Boolean
                                            , silent: Boolean = false
    ): Boolean {
        val statement: PsiBuilder.Marker = myBuilder.mark()
        parseKey()
        println("In parse keyValue at token ${myBuilder.tokenType} after parseKey")
        // Statement should not exist when there is no ":"
        if (silent) {
            if (!matchToken(CwlTokenTypes.COLON)) return false.also { statement.drop() }
        } else {
            if (!checkMatches(CwlTokenTypes.COLON, CwlBundle.message("PARSE.expected.colon"))) return false.also { statement.drop() }
        }
        println("In parse keyValue at token ${myBuilder.tokenType} after check colon")
        // Wrong or empty value is normal
        return parseValue().also {
            statement.done(statementElement)
        }
    }


    protected inline fun checkFullStatementWithSimpleKey(statementElement: IElementType = CwlElementTypes.STATEMENT
                                                         , key: IElementType
                                                         , silent: Boolean = false
                                                         , parseValue: () -> Boolean
    ): Boolean =
            checkFullStatement(parseKey = { checkMatches(key, "$key expected") }
                    , parseValue = parseValue
                    , statementElement = statementElement
                    , silent = silent
            )

    /**
     * Parse statement when key is already checked
     *
     * Checked key is just skipped by nextToken()
     */
    protected inline fun checkStatement(statementElement: IElementType = CwlElementTypes.STATEMENT
                                        , silent: Boolean = false
                                        , parseValue: () -> Boolean
    ): Boolean =
            checkFullStatement(parseKey = { nextToken(); true }
                    , parseValue = parseValue
                    , statementElement = statementElement
                    , silent = silent
            )

    /**
     * Parse statements with value as simple token
     */
    protected fun parseSimpleStatement(secondToken: IElementType
                                       , silent: Boolean = false
                                       , statementElement: IElementType = CwlElementTypes.STATEMENT
    ): Boolean =
            checkStatement(statementElement = statementElement
                    , parseValue = {
                checkMatches(secondToken, "$secondToken expected")
                checkMatches(CwlTokenTypes.LINE_BREAK, "Line break expected")
            }
                    , silent = silent
            )


    protected inline fun checkValue(statementElement: IElementType = CwlElementTypes.STATEMENT
                                    , silent: Boolean = false
                                    , parseValue: () -> Boolean
    ): Boolean =
            checkStatement(statementElement = statementElement
                    , silent = silent
                    , parseValue = parseValue)

    protected inline fun parseFlowSequence(blockType: IElementType = CwlElementTypes.FLOW_SEQUENCE
                                           , parseElement: () -> Boolean
    ): Boolean {

        parsingContext.pushScope(parsingContext.currentScope.withFlowSequence())
        println("Flow sequence in ${parsingContext.currentScope} is ${parsingContext.currentScope.inFlowSequence}")
        val block = myBuilder.mark()
        nextToken() // Skip left bracket that is already checked
        with(CwlTokenTypes) {
            if (!parseElement()) {
                myBuilder.error("Empty sequence")
                return false.also { block.drop() }
            }
            while (!atToken(RBRACKET)) {
                if (!checkMatches(COMMA, CwlBundle.message("PARSE.expected.comma"))) return false.also { block.drop() }
                if (!parseElement()) {
//                    myBuilder.error("One of $elementTypes expected")
                }
            }
            if (!checkMatches(RBRACKET, CwlBundle.message("PARSE.expected.rbracket"))) return false.also { block.drop() }
        }
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                .also {
                    block.done(blockType)
                    parsingContext.popScope()
                }
    }

    protected fun parseSimpleFlowSequence(blockType: IElementType = CwlElementTypes.FLOW_SEQUENCE
                                          , vararg elementTypes: IElementType
    ): Boolean =
            parseFlowSequence(blockType) { matchAnyOfTokens(*elementTypes) }

    protected fun parseFlowMapping(parseStatement: () -> Boolean
    ): Boolean {
        nextToken()
        with(CwlTokenTypes) {
            if (!checkMatches(COLON, CwlBundle.message("PARSE.expected.colon"))) return false
            if (!checkMatches(LBRACE, CwlBundle.message("PARSE.expected.lbracket"))) return false
            if (!parseStatement()) return false
            while (!atToken(RBRACE)) {
                println("in do statement")
                if (!checkMatches(COMMA, CwlBundle.message("PARSE.expected.comma"))) return false
                if (!parseStatement()) return false
            }
            if (!checkMatches(RBRACE, CwlBundle.message("PARSE.expected.rbracket"))) return false
        }
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
    }

    protected fun parseIntValue(): Boolean {
        checkMatches(CwlTokenTypes.INT, CwlBundle.message("PARSE.expected.int"))
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
    }

    protected fun parseBoolValue(): Boolean {
        checkMatches(CwlTokenTypes.BOOLEAN, CwlBundle.message("PARSE.expected.bool"))
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
    }

    protected fun parseStringValue(silent: Boolean = false): Boolean = parseStringValueOrSmth {
        if (!silent) myBuilder.error(CwlBundle.message("PARSE.expected.string"))
        false
    }

    protected fun parseStringValueOrSmth(parseSmth: () -> Boolean
    ): Boolean {

        with(CwlTokenTypes) {
            when (myBuilder.tokenType) {
                PIPE -> {
                    nextToken()
                    val multiLineString: PsiBuilder.Marker = myBuilder.mark()
                    if (!parseMultiLineString()) {
                        multiLineString.drop()
                        return false
                    }
                    multiLineString.done(CwlElementTypes.MULTI_LINE_STRING)
                    return true
                }
                STRING -> {
                    println("In parse string at token ${myBuilder.tokenType}")
                    nextToken()
                    println("In parse string at token ${myBuilder.tokenType}")
                    return if (!parsingContext.currentScope.inFlowSequence) {
                        checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    } else true

                }
                else -> {
                    return parseSmth()
                }
            }
        }
    }

    protected fun parseMultiLineString(): Boolean {
        with(CwlTokenTypes) {
            if (!checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))) return false
            if (!checkMatches(INDENT, "Indent expected")) return false
            if (!checkMatches(MLSPART, "Start of mls expected")) return false
            if (!checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))) return false
            while (!atToken(DEDENT) && !myBuilder.eof()) {
                println("in do statement")
                if (!checkMatches(MLSPART, "Dedent or mls expected")) return false
                if (!checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))) return false
            }
            // At the end of the file dedent is not necessary
            if (myBuilder.eof()) return true
            if (!checkMatches(DEDENT, "End of mls expected")) return false
        }
        return true
    }

    protected fun parseSimpleGeneralSequence(blockType: IElementType = CwlElementTypes.SEQUENCE
                                             , elementType: CwlElementType
    ): Boolean {
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                LBRACKET -> {
                    parseSimpleFlowSequence(blockType, elementType)
                }
                LINE_BREAK -> {
                    parseSimpleSequence(blockType, elementType)
                }
                else -> {
                    myBuilder.error("Array of $elementType is expected")
                    false
                }
            }
        }
    }

    protected fun parseGeneralSequence(blockType: IElementType? = null
                                       , parseElement: () -> Boolean
    ): Boolean {
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                LBRACKET -> {
                    parseFlowSequence(blockType ?: CwlElementTypes.FLOW_SEQUENCE, parseElement)
                }
                LINE_BREAK -> {
                    parseSequence(blockType ?: CwlElementTypes.SEQUENCE, parseElement)
                }
                else -> {
                    myBuilder.error("Array of ... is expected")
                    false
                }
            }
        }
    }

    protected inline fun parseEither(firstToken: IElementType
                                     , parseFirstElement: () -> Boolean
                                     , secondToken: IElementType
                                     , parseSecondElement: () -> Boolean
    ) {
        when (myBuilder.tokenType) {
            firstToken -> {
                parseFirstElement()
            }
            secondToken -> {
                parseSecondElement()
            }
            else -> {
                myBuilder.error(CwlBundle.message("PARSE.expected.string_or_array"))
            }
        }
    }

    protected inline fun parseEither(firstToken: IElementType
                                     , parseFirstElement: () -> Boolean
                                     , parseSecondElement: () -> Boolean
    ): Boolean =
            when (myBuilder.tokenType) {
                firstToken -> {
                    parseFirstElement()
                }
                else -> {
                    parseSecondElement()
                }
            }

    protected inline fun parseEither(parseFirstElement: () -> Boolean
                                     , parseSecondElement: () -> Boolean
    ): Boolean =
            if (parseFirstElement()) true else parseSecondElement()

    protected inline fun parseEither(firstToken: IElementType
                                     , parseFirstElement: () -> Boolean
                                     , secondToken: IElementType
                                     , parseSecondElement: () -> Boolean
                                     , thirdToken: IElementType
                                     , parseThirdElement: () -> Boolean) {
        when (myBuilder.tokenType) {
            firstToken -> {
                parseFirstElement()
            }
            secondToken -> {
                parseSecondElement()
            }
            thirdToken -> {
                parseThirdElement()
            }
            else -> {
                myBuilder.error(CwlBundle.message("PARSE.expected.string_or_array"))
            }
        }
    }

    protected inline fun parseMap(blockType: IElementType = CwlElementTypes.MAP
                                  , noinline parseFirstStatement: (() -> Boolean)? = null
                                  , parseStatement: () -> Boolean
    ): Boolean {
        val currentScope = parsingContext.currentScope
        println("\nScope in map: ${currentScope.inMap}; in seq: ${currentScope.inSequence}")
        parsingContext.pushScope(currentScope.withMap())
        val map: PsiBuilder.Marker = myBuilder.mark()
        when {
            currentScope.inMap -> {
                println("Inside map")
                if (!checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))) {
                    return false.also {
                        map.drop()
                        parsingContext.popScope()
                    }
                }
                if (!checkMatches(CwlTokenTypes.INDENT, CwlBundle.message("PARSE.expected.indent"))) {
                    return false.also {
                        map.drop()
                        parsingContext.popScope()
                    }
                }
            }
        }
        if (currentScope.inMap || currentScope.inSequence) {
            println("Before first statement parse at token ${myBuilder.tokenType}")
            if (parseFirstStatement != null) parseFirstStatement()
            else parseStatement()
        }
        println("After first statement parse")
        if (!currentScope.inMap && !currentScope.inSequence) {
            parseFirstStatement?.invoke()
            while (!myBuilder.eof()) {
                if (!parseStatement()) {
                    nextToken()
                }
            }
            println("\nI must be the only one!")
            return true.also {
                map.done(blockType)
                parsingContext.popScope()
            }
        } else {
            println(" Why i got here at token ${myBuilder.tokenType}")
            // TODO think better about inconsistent dedents
            while (!myBuilder.eof() && !atToken(CwlTokenTypes.DEDENT) && !atToken(CwlTokenTypes.INCONSISTENT_DEDENT)) {
//            myBuilder.error("Start parse statement at token ${myBuilder.tokenType}")
                if (!parseStatement()) {
                    myBuilder.error("Wrong statement at token ${myBuilder.tokenType}")
                    if (atToken(CwlTokenTypes.DEDENT)) {
                        break
                    }
                    println("Statement is not parsed skip token ${myBuilder.tokenType}")
                    nextToken()
                } // Skip tokens until dedent if nothing matches
            }
            println("Escaped map!")
            return (if (!myBuilder.eof() && !currentScope.inSequence) {
                checkMatches(CwlTokenTypes.DEDENT, "Dedent expected")
            } else true).also {
                map.done(blockType)
                parsingContext.popScope()
            }
        }
    }

    protected fun parseExpression(): Boolean =
            if (matchToken(CwlTokenTypes.EXPRESSION)) {
                checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
            } else false

    protected fun parseStringOrExpression(): Boolean = parseStringValueOrSmth { parseExpression() }

    protected fun parseStringOrExpressionOrInt(): Boolean =
            parseEither(firstToken = CwlTokenTypes.INT, parseFirstElement = this::parseIntValue,
                    parseSecondElement = this::parseStringOrExpression)

    protected fun parseTypeValue(): Boolean {
        with(CwlTokenTypes) {
            val type = myBuilder.mark()
            return when (myBuilder.tokenType) {
                LINE_BREAK -> {
                    val token: IElementType? = myBuilder.lookAhead(2) ?: return false
                    when (token) {
                        SEQUENCE_ELEMENT_PREFIX -> parseSequenceTypes(this@Parsing::parseSchemas)
                        else -> parseSchemas()
                    }
                }
                LBRACKET -> parseTypesFlowSequence()
                else -> parseStringValueOrSmth { parseSimpleType() }
            }.also { type.done(CwlElementTypes.CWL_TYPE) }
        }
    }

    protected fun parseSimpleType(): Boolean {

        with(parsingContext.currentScope) {
            if (inCommandLineTool) {
                when {
                    inInputs -> if (matchToken(CwlTokenTypes.STDIN_KEYWORD)) {
                        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    }
                    inOutputs -> if (matchAnyOfTokens(CwlTokenTypes.STDOUT_KEYWORD, CwlTokenTypes.STDERR_KEYWORD)) {
                        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    }
                }
            }
        }
        if (matchAnyOfTokens(*CwlTokenTypes.CWL_TYPES.types)) else myBuilder.error("Type expected")
        if (matchToken(CwlTokenTypes.LBRACKET)) {
            checkMatches(CwlTokenTypes.RBRACKET, CwlBundle.message("PARSE.expected.rbracket"))
        }
        matchToken(CwlTokenTypes.QUESTION_MARK)
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
    }


    protected fun parseTypesFlowSequence(): Boolean =
            parseSimpleFlowSequence(elementTypes = *CwlTokenTypes.CWL_TYPES.types.plus(CwlTokenTypes.STRING))

    protected inline fun parseSequenceTypes(parseSchemas: () -> Boolean
    ): Boolean {
        return parseSequence(parseElement = {
            when {
                atToken(CwlTokenTypes.STRING) -> parseStringValue()
                atAnyOfTokens(*CwlTokenTypes.CWL_TYPES.types) -> parseSimpleType()
                else -> parseSchemas()
            }
        })
    }

    protected fun parseSchemas(): Boolean {
        /*
        When schema is the only item in the map it has line break and indent before type, hence
        lookAheadStep is increased. Maybe parseMap should be reconsidered, and parseKeyValue and map should be united.
         */
        val lookAheadStep = if (parsingContext.currentScope.inSequence) 2 else 4
        val token: IElementType? = myBuilder.lookAhead(lookAheadStep) ?: return false
        println("parseSchemas lookup $token")
        return when (token) {
            CwlTokenTypes.ENUM_TYPE_KEYWORD -> parsingContext.requirementsParser.parseEnumSchema()
            CwlTokenTypes.ARRAY_TYPE_KEYWORD -> parsingContext.requirementsParser.parseArraySchema()
            CwlTokenTypes.RECORD_KEYWORD -> parsingContext.requirementsParser.parseRecordSchema()
            else -> return false
        }
    }

    protected fun matchAnyOfTokens(vararg tokenTypes: IElementType
    ): Boolean =
            if (tokenTypes.any { myBuilder.tokenType === it }) {
            nextToken()
            true
        } else false

    protected inline fun parseSingleIndentedStatement(parseStatement: () -> Boolean
    ): Boolean {
        return with(CwlTokenTypes) {
            if (parsingContext.currentScope.inMap) {
                checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
            }
            checkMatches(INDENT, CwlBundle.message("PARSE.expected.indent"))
            println("Before parse statement in single indent statement: ${myBuilder.tokenType}")
            parseStatement().also {
                if (!myBuilder.eof()) checkMatches(DEDENT, CwlBundle.message("PARSE.expected.dedent"))
            }
        }
    }

    fun parseAny(): Boolean {
        val tokenType = myBuilder.tokenType ?: return false
        when (tokenType) {
            CwlTokenTypes.LINE_BREAK -> {
                /*
                Parse map or sequence
                 */
                val lookAheadTokenType = myBuilder.lookAhead(2) ?: return false
                return when (lookAheadTokenType) {
                // TODO sequences in sequences are valid?
                    CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX -> parseSequence(parseElement = { parseAny() })
                    else -> parseMap(parseStatement = { checkStatement(parseValue = { parseAny() }) })
                }
            }
            else -> {
                if (parsingContext.currentScope.inSequence) {
                    /*
                    In sequence map starts at the same line, and it can be distinguished only by colon
                    If map is found just return its parsing result, otherwise continue with simple line parsing
                     */
                    val lookAheadTokenType = myBuilder.lookAhead(1) ?: return false
                    when (lookAheadTokenType) {
                        CwlTokenTypes.COLON -> return parseMap(parseStatement = { checkStatement(parseValue = { parseAny() }) })
                    }
                }
                /*
                Skip all tokens until line break
                 */
                // TODO Maybe add flow sequence later?
                while (myBuilder.tokenType != CwlTokenTypes.LINE_BREAK && !myBuilder.eof()) {
                    nextToken()
                }
                return matchToken(CwlTokenTypes.LINE_BREAK)
            }
        }
    }

    protected fun parseSaladDirective(): Boolean = with(CwlTokenTypes) {

        checkFullStatement(parseKey = { matchAnyOfTokens(IMPORT_KEYWORD, INCLUDE_KEYWORD, MIXIN_KEYWORD) }
                , parseValue = { parseStringValue(silent = true) }
                , silent = true
        )
    }
}