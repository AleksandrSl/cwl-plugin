package org.cwlplugin.parser

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
enum class ToolType {

    COMMAND_LINE_TOOL,
    EXPRESSION_TOOL,
    WORKFLOW;
}