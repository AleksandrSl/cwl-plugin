package org.cwlplugin.parser

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class ParsingScope {

    var inSequence = false
        private set(v) {
            if (v) {
                inMap = false
            }
            field = v
        }

    var inMap: Boolean = false
        private set(v) {
            if (v) {
                inSequence = false
            }
            field = v
        }

    var inOutputs: Boolean = false
        private set

    var inInputs: Boolean = false
        private set

    var inWorkflow: Boolean = false
        private set(v) {
            if (v) {
                inStep = false
            }
            field = v
        }

    var inStep: Boolean = false
        private set

    // Set step to false when new tool inside it is created
    var inCommandLineTool: Boolean = false
        private set(v) {
            if (v) {
                inStep = false
            }
            field = v
        }

    // Set step to false when new tool inside it is created
    var inFlowSequence: Boolean = false
        private set(v) {
            if (v) {
                inMap = false
            }
            field = v
        }

    // Set step to false when new tool inside it is created
    var inExpressionTool: Boolean = false
        private set(v) {
            if (v) {
                inStep = false
            }
            field = v
        }

    fun withSequence(): ParsingScope = copy().apply { inSequence = true }

    fun withInputs(): ParsingScope = copy().apply { inInputs = true }

    fun withOutputs(): ParsingScope = copy().apply { inOutputs = true }

    fun withCommandLineTool(): ParsingScope = copy().apply { inCommandLineTool = true }

    fun withWorkflow(): ParsingScope = copy().apply { inWorkflow = true }

    fun withExpressionTool(): ParsingScope = copy().apply { inExpressionTool = true }

    fun withMap(): ParsingScope = copy().apply { inMap = true }

    fun withStep(): ParsingScope = copy().apply { inStep = true }

    fun withFlowSequence(): ParsingScope = copy().apply { inFlowSequence = true }

    protected fun createInstance(): ParsingScope {
        return ParsingScope()
    }

    protected fun copy(): ParsingScope {
        val result = createInstance().apply {
            inInputs = this@ParsingScope.inInputs
            inMap = this@ParsingScope.inMap
            inSequence = this@ParsingScope.inSequence
            inOutputs = this@ParsingScope.inOutputs
            inWorkflow = this@ParsingScope.inWorkflow
            inStep = this@ParsingScope.inStep
            inCommandLineTool = this@ParsingScope.inCommandLineTool
        }
        return result
    }
}