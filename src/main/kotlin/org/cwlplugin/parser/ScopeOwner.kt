package org.cwlplugin.parser

import org.cwlplugin.psi.CwlElement

/**
 * Marker interface for CWL elements which define a scope.
 *
 * @see ControlFlowCache
 * @author oleg, Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface ScopeOwner: CwlElement