package org.cwlplugin.parser

import org.cwlplugin.psi.CwlElementTypes

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class ExpressionToolParser(context: ParsingContext) : CommandLineToolParser(context) {

    fun parseExpressionToolField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        return with(CwlTokenTypes) {
            when (firstToken) {
                REQUIREMENTS_KEYWORD -> {
                    // Why expression tool has the same requirements as workflow?
                    parsingContext.requirementsParser.parseWorkflowRequirementsBlock()
                }
                INPUTS_KEYWORD -> {
                    checkValue(CwlElementTypes.INPUTS) { parseInputs() }
                }
                OUTPUTS_KEYWORD -> {
                    checkValue(CwlElementTypes.OUTPUTS) { parseOutputs() }
                }
                EXPRESSION_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                LABEL_KEYWORD, DOC_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                HINTS_KEYWORD -> {
                    checkValue { parseSequence { parseAny() } }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken)
                    false
                }
            }
        }
    }
}