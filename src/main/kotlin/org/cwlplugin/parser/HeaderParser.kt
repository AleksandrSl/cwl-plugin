package org.cwlplugin.parser

import org.cwlplugin.CwlBundle
import org.cwlplugin.psi.CwlElementTypes

/**
 * Parse elements at CWL header
 * Created by aleksandrsl on 05.07.17.
 */
class HeaderParser(val context: ParsingContext) : Parsing(context) {

    // TODO: think about other way to organize different tools parsing, without constraining first two lines
    // to cwlVersion and class
    fun parseHeaderElements(): Boolean {

        with(CwlTokenTypes) {
            checkValue(CwlElementTypes.VERSION) { parseCwlVersionValue() }
            return parseTool()
        }
    }

    fun parseTool(): Boolean {

        val tokenType = myBuilder.lookAhead(if (context.currentScope.inMap) 4 else 2) ?: return false
        with(CwlTokenTypes) {
            when (tokenType) {
                COMMAND_LINE_TOOL_KEYWORD -> {
                    val commandLineToolParser = context.commandLineToolParser
                    context.pushScope(context.currentScope.withCommandLineTool())
                    parseMap(
                            parseFirstStatement = {
                                checkFullStatementWithSimpleKey(key = CLASS_KEYWORD) {
                                    nextToken()
                                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                                }
                            }
                            , parseStatement = { commandLineToolParser.parseCommandLineToolField() }
                            , blockType = CwlElementTypes.COMMAND_LINE_TOOL_CLASS
                    )
                    context.popScope()
                }
                WORKFLOW_KEYWORD -> {
                    val workflowParser = context.workflowParser
                    context.pushScope(context.currentScope.withWorkflow())
                    parseMap(
                            parseFirstStatement = {
                                checkFullStatementWithSimpleKey(key = CLASS_KEYWORD) {
                                    nextToken()
                                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                                }
                            }
                            , parseStatement = { workflowParser.parseWorkflowField() }
                            , blockType = CwlElementTypes.WORKFLOW_CLASS
                    )
                    context.popScope()
                }
                EXPRESSION_TOOL_KEYWORD -> {
                    val expressionToolParser = context.expressionToolParser
                    context.pushScope(context.currentScope.withExpressionTool())
                    parseMap(
                            parseFirstStatement = {
                                checkFullStatementWithSimpleKey(key = CLASS_KEYWORD) {
                                    nextToken()
                                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                                }
                            }
                            , parseStatement = { expressionToolParser.parseExpressionToolField() }
                            , blockType = CwlElementTypes.EXPRESSION_TOOL_CLASS
                    )
                    context.popScope()
                }
                else -> {
                    myBuilder.error(CwlBundle.message("PARSE.expected.tool"))
                    return false
                }
            }
        }
        return true
    }

    fun parseCwlVersionValue(): Boolean {

        if (!atToken(CwlTokenTypes.STRING, "v1.0")) {
            myBuilder.error(CwlBundle.message("PARSE.invalid.version"))
        }
        nextToken()
        return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
    }
}