package org.cwlplugin.parser

import com.intellij.openapi.diagnostic.Logger
import com.intellij.psi.tree.IElementType
import org.cwlplugin.CwlBundle
import org.cwlplugin.CwlBundle.message
import org.cwlplugin.psi.CwlElementTypes

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class RequirementsParser(context: ParsingContext) : Parsing(context) {

    private val LOG = Logger.getInstance(RequirementsParser::class.java.name)

    /**
     *
     */
    fun parseRequirementsBlock(): Boolean = checkValue(CwlElementTypes.REQUIREMENTS_BLOCK) {
        parseSequence(parseElement = this::parseRequirementStatement)
    }

    /**
     * Parse requirement statement
     *
     *  class: requirement
     */
    protected fun parseRequirementStatement(): Boolean {

        println("parseRequirement:: ${myBuilder.tokenType}")
        val tokenType = myBuilder.lookAhead(2) ?: return false
        println("lookAhead $tokenType")
        with(CwlTokenTypes) {
            when (tokenType) {
                INLINE_JAVASCRIPT_REQUIREMENT_KEYWORD -> {
                    parseInlineJavascriptRequirement()
                }
                DOCKER_REQUIREMENT_KEYWORD -> {
                    parseDockerRequirement()
                }
                SCHEMA_DEF_REQUIREMENT_KEYWORD -> {
                    parsingContext.pushScope(parsingContext.currentScope.withInputs())
                    parseSchemaDefRequirement()
                    parsingContext.popScope()
                }
                INITIAL_WORKDIR_REQUIREMENT_KEYWORD -> {
                    parseInitialWorkDirRequirement()
                }
                RESOURCE_REQUIREMENT_KEYWORD -> {
                    parseResourceRequirement()
                }
                ENV_VAR_REQUIREMENT_KEYWORD -> {
                    parseEnvVarRequirement()
                }
                SHELL_COMMAND_REQUIREMENT_KEYWORD -> {
                    parseMap(parseStatement = { parseRequirementHeader(SHELL_COMMAND_REQUIREMENT_KEYWORD) })
                }
                SOFTWARE_REQUIREMENT_KEYWORD -> {
                    parseSoftwareRequirement()
                }
                else -> {
                    myBuilder.error("Requirement expected")
                    println("Requirement token : $tokenType")
                    return false
                }
            }
            return true
        }
    }

    protected fun parseDockerRequirement(): Boolean {
        println("Docker requirement scope inSequence: ${parsingContext.currentScope.inSequence}")
        return parseMap(
                blockType = CwlElementTypes.DOCKER_REQUIREMENT
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.DOCKER_REQUIREMENT_KEYWORD) }
                , parseStatement = this@RequirementsParser::parseDockerRequirementField
        )
    }

    protected fun parseDockerRequirementField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                DOCKER_FILE_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DOCKER_IMAGE_ID_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DOCKER_IMPORT_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DOCKER_LOAD_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DOCKER_PULL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DOCKER_OUTPUT_DIRECTORY_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> {
                    false
                }
            }
        }
    }

    protected fun parseInlineJavascriptRequirement(): Boolean {
        println("I got here")
        return parseMap(
                blockType = CwlElementTypes.INLINE_JAVASCRIPT_REQUIREMENT
                , parseStatement = {
            if (atToken(CwlTokenTypes.EXPRESSION_LIB_KEYWORD)) {
                checkValue {
                    parseSimpleGeneralSequence(CwlElementTypes.SEQUENCE, CwlTokenTypes.STRING)
                }
            } else false
        }
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.INLINE_JAVASCRIPT_REQUIREMENT_KEYWORD) }
        )
    }

    /*
     * What the hell is this
     * I hate ambiguous syntax
     */
    protected fun parseSoftwareRequirement(): Boolean {
        return parseMap(
                blockType = CwlElementTypes.SOFTWARE_REQUIREMENT,
                parseStatement = {
                    if (atToken(CwlTokenTypes.PACKAGES_KEYWORD)) {
                        val tokenType = myBuilder.lookAhead(4) ?: return@parseMap false
                        println("Raw token $tokenType")
                        when (tokenType) {
                        // array<SoftwarePackage>
                            CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX -> {
                                checkValue {
                                    parseSequence(parseElement = {
                                        parseMap(
                                                parseStatement = this::parseSoftwarePackageField
                                                , blockType = CwlElementTypes.SOFTWARE_PACKAGE
                                        )
                                    }).also { println("parse sequence inside packages result $it") }
                                }
                            }
                            CwlTokenTypes.STRING -> {
                                val lookAheadToken = myBuilder.lookAhead(8)
                                if (lookAheadToken != null) {
                                    println("Raw token inside $lookAheadToken")
                                    when (lookAheadToken) {
                                    // map<SoftwarePackage.package, SoftwarePackage>
                                        CwlTokenTypes.PACKAGE_KEYWORD, // Is it probable?
                                        CwlTokenTypes.VERSION_KEYWORD,
                                        CwlTokenTypes.SPECS_KEYWORD -> {
                                            println("Inside software package")
                                            checkValue {
                                                parseMap(parseStatement = this::parseSoftwarePackage
                                                        , blockType = CwlElementTypes.SOFTWARE_PACKAGE
                                                )
                                            }
                                        }
                                    // map<SoftwarePackage.package, SoftwarePackage.specs>
                                        else -> {
                                            checkValue(CwlElementTypes.SOFTWARE_PACKAGE) {
                                                parseSingleIndentedStatement(
                                                        parseStatement = { checkValue { parseSimpleGeneralSequence(CwlElementTypes.SEQUENCE, CwlTokenTypes.STRING) } }
                                                )
                                            }
                                        }
                                    }
                                } else false
                            }
                            else -> false
                        }
                    } else false
                }
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.SOFTWARE_REQUIREMENT_KEYWORD) }
        )
    }

    protected fun parseSoftwarePackage(): Boolean {
        return if (myBuilder.tokenType == CwlTokenTypes.STRING) {
            checkValue {
                parseMap(
                        parseStatement = this::parseSoftwarePackageField
                )
            }
        } else {
            myBuilder.error("Software Package expected")
            false
        }
    }

    protected fun parseSoftwarePackageField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                PACKAGE_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                VERSION_KEYWORD, SPECS_KEYWORD -> {
                    checkValue {
                        parseSimpleGeneralSequence(CwlElementTypes.SEQUENCE, CwlTokenTypes.STRING)
                    }
                }
                else -> {
                    myBuilder.error("Software Package field expected")
                    false
                }
            }
        }
    }

    protected fun parseResourceRequirement(): Boolean {
        return parseMap(
                blockType = CwlElementTypes.RESOURCE_REQUIREMENT
                , parseStatement = this@RequirementsParser::parseResourceRequirementField
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.RESOURCE_REQUIREMENT_KEYWORD) }
        )
    }

    protected fun parseResourceRequirementField(): Boolean {
        with(CwlTokenTypes) {
            return when {
                atAnyOfTokens(CORES_MAX_KEYWORD, CORES_MIN_KEYWORD, RAM_MAX_KEYWORD, RAM_MIN_KEYWORD,
                        TMPDIR_MAX_KEYWORD, TMPDIR_MIN_KEYWORD, OUTDIR_MAX_KEYWORD, OUTDIR_MIN_KEYWORD) -> {
                    checkValue { parseStringOrExpressionOrInt() }
                }
                else -> false
            }
        }
    }

    protected fun parseInitialWorkDirRequirement(): Boolean {
        return parseMap(
                blockType = CwlElementTypes.INITIAL_WORKDIR_REQUIREMENT
                // array<File | Directory | Dirent | string | Expression> | string | Expression
                , parseStatement = {
            if (atToken(CwlTokenTypes.LISTING_KEYWORD)) {
                checkValue { ->
                    parseInitialWorkDirListingStatement()
                }
            } else false
        }
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.INITIAL_WORKDIR_REQUIREMENT_KEYWORD) }
        )
    }

    protected fun parseInitialWorkDirListingStatement(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                STRING -> {
                    println("In simple string")
                    nextToken()
                    return checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                }
                EXPRESSION -> {
                    println("In simple expr")
                    parseExpression()
                }
                LBRACKET -> {
                    // Presume that File and string will not be in flow seq
                    println("In flow seq")
                    parseSimpleFlowSequence(elementTypes = *arrayOf(STRING, EXPRESSION))
                }
                LINE_BREAK -> {
                    // File | Directory | Dirent | string | Expression
                    println("In seq")
                    parseSequence(
                            parseElement = this@RequirementsParser::parseInitialWorkDirListingSequenceStatement
                    )
                }
                else -> {
                    false
                }
            }
        }
    }

    protected fun parseInitialWorkDirListingSequenceStatement(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                STRING -> {
                    println("In string inside seq")
                    nextToken()
                    return checkMatches(LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                }
                EXPRESSION -> {
                    println("In expr inside seq")
                    parseExpression()
                }
            // File | Directory
                CLASS_KEYWORD -> {
                    println("In file/dir inside seq")
                    parseFile()
                }
                ENTRY_KEYWORD -> {
                    println("In dirent inside seq")
                    parseDirent()
                }
                else -> false
            }
        }
    }

    protected fun parseDirectory(): Boolean = parseMap(
            parseFirstStatement = {
                checkFullStatementWithSimpleKey(key = CwlTokenTypes.CLASS_KEYWORD) {
                    checkMatches(CwlTokenTypes.DIRECTORY_TYPE_KEYWORD, "Directory expected")
                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                }
            }
            , parseStatement = { parseDirectoryField() }
            , blockType = CwlElementTypes.DIRECTORY
    )

    protected fun parseDirectoryField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                LOCATION_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                PATH_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                BASENAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                LISTING_KEYWORD -> {
                    checkValue {
                        parseSequence(parseElement = this@RequirementsParser::parseFileOrDirectory)
                    }
                }
                else -> {
                    false
                }
            }
        }
    }

    protected fun parseFile(): Boolean = parseMap(
            parseFirstStatement = {
                checkFullStatementWithSimpleKey(key = CwlTokenTypes.CLASS_KEYWORD) {
                    checkMatches(CwlTokenTypes.FILE_TYPE_KEYWORD, "File expected")
                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                }
            }
            , parseStatement = {
        parseFileField()
    }
            , blockType = CwlElementTypes.FILE
    )

    protected fun parseFileField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                LOCATION_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                PATH_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                BASENAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                DIRNAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                NAMEROOT_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                NAMEEXT_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                CHECKSUM_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                SIZE_KEYWORD -> {
                    checkValue { parseIntValue() }
                }
                SECONDARY_FILES_KEYWORD -> {
                    checkValue {
                        parseSequence(
                                parseElement = this@RequirementsParser::parseFileOrDirectory
                        )
                    }
                }
                FORMAT_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                CONTENTS_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> {
                    false
                }
            }
        }
    }

    protected fun parseFileOrDirectory(): Boolean {
        // TODO: Maybe better way exists without rawLookup
        myBuilder.lookAhead(2)?.let {
            println("lookAhead in parseFileOrDirectory $it")
            return when (it) {
                CwlTokenTypes.DIRECTORY_TYPE_KEYWORD -> {
                    parseDirectory()
                }
                CwlTokenTypes.FILE_TYPE_KEYWORD -> {
                    parseFile()
                }
                else -> false
            }
        }
        return false
    }

    protected fun parseDirent(): Boolean = parseMap(
            parseStatement = this::parseDirentField
            , blockType = CwlElementTypes.DIRENT
    )

    protected fun parseDirentField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                ENTRY_KEYWORD -> {
                    println("In entry at token ${myBuilder.tokenType}")
                    checkValue {
                        parseStringOrExpression()
                    }
                }
                ENTRYNAME_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                WRITABLE_KEYWORD -> {
                    checkValue { parseBoolValue() }
                }
                else -> false
            }
        }
    }

    protected fun parseEnvironmentDef(): Boolean {
        return if (myBuilder.tokenType == CwlTokenTypes.STRING) {
            checkValue {
                parseMap(parseStatement = this::parseEnvironmentDefField)
            }
        } else {
            myBuilder.error("EnvironmentDef expected")
            return false
        }
    }

    protected fun parseEnvironmentDefField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                ENV_NAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                ENV_VALUE_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                else -> false
            }
        }
    }

    protected fun parseEnvVarRequirement(): Boolean {
        return parseMap(
                blockType = CwlElementTypes.ENV_VAR_REQUIREMENT
                , parseStatement = {
            if (atToken(CwlTokenTypes.ENV_DEF_KEYWORD)) {
                var tokenType = myBuilder.lookAhead(4) ?: return@parseMap false
                println("Token $tokenType")
                when (tokenType) {
                // array<EnvironmentDef>
                    CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX -> {
                        checkValue {
                            parseSequence(parseElement = {
                                parseMap(
                                        parseStatement = this::parseEnvironmentDefField
                                        , blockType = CwlElementTypes.ENV_DEF
                                )
                            }
                            )
                        }
                    }
                    CwlTokenTypes.STRING -> {
                        tokenType = myBuilder.lookAhead(8) ?: return@parseMap false
                        println("Raw token inside $tokenType")
                        when (tokenType) {
                        // map<EnvironmentDef.envName, EnvironmentDef>
                            CwlTokenTypes.ENV_NAME_KEYWORD, // Is it probable?
                            CwlTokenTypes.ENV_VALUE_KEYWORD -> {
                                println("Inside env def ")
                                checkValue { ->
                                    parseMap(parseStatement = this::parseEnvironmentDef
                                            , blockType = CwlElementTypes.ENV_DEF
                                    )
                                }
                            }
                        // map<EnvironmentDef.envName, EnvironmentDef.envValue>
                            else -> {
                                checkValue(CwlElementTypes.ENV_DEF) {
                                    parseSingleIndentedStatement(parseStatement = {
                                        checkFullStatementWithSimpleKey(key = CwlTokenTypes.STRING) { parseStringOrExpression() }
                                    }
                                    )
                                }
                            }
                        }
                    }
                    else -> false
                }
            } else false
        }
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.ENV_VAR_REQUIREMENT_KEYWORD) }
        )
    }

    protected fun parseSchemaDefRequirement(): Boolean {
        return parseMap(
                blockType = CwlElementTypes.SCHEMA_DEF_REQUIREMENT
                , parseStatement = {
            if (atToken(CwlTokenTypes.TYPES_KEYWORD)) {
                checkValue {
                    // Should output schemas be enabled?
                    parseSequence(parseElement = this::parseSchemas)
                }
                true
            } else false
        }
                , parseFirstStatement = { parseRequirementHeader(CwlTokenTypes.SCHEMA_DEF_REQUIREMENT_KEYWORD) }
        )
    }

    fun parseRecordSchema(): Boolean = parseMap(
            parseStatement = {
                with(parsingContext.currentScope) {
                    when {
                        inInputs -> parseRecordSchemaField(this@RequirementsParser::parseInputRecordField)
                        inOutputs -> parseRecordSchemaField(this@RequirementsParser::parseOutputRecordField)
                        else -> false
                    }
                }
            }
            , blockType = CwlElementTypes.RECORD_SCHEMA
    )

    protected fun parseRecordSchemaField(parseField: () -> Boolean): Boolean {
        myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                TYPE_KEYWORD -> {
                    checkValue(parseValue =
                    {
                        // TODO: change to RECORD_TYPE_KEYWORD?
                        checkMatches(CwlTokenTypes.RECORD_KEYWORD, "record expected")
                        checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    }
                    )
                }
                FIELDS_KEYWORD -> {
                    checkValue {
                        parseSequence(
                                parseElement = parseField
                        )
                    }
                }
                LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> false
            }
        }
    }

    protected fun parseInputRecordField(): Boolean = parseMap(parseStatement = this::parseInputRecordSubfield
            , blockType = CwlElementTypes.RECORD_FIELD
    )

    protected fun parseInputRecordSubfield(): Boolean {
        myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                NAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                TYPE_KEYWORD -> {
                    checkValue { parseSimpleType() }
                }
                INPUT_BINDING_KEYWORD -> {
                    checkValue(CwlElementTypes.INPUT_BINDING) { parsingContext.commandLineToolParser.parseInputBindingFields() }
                }
                DOC_KEYWORD, LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> false
            }
        }
    }

    protected fun parseOutputRecordField(): Boolean = parseMap(parseStatement = this::parseOutputRecordSubfield
            , blockType = CwlElementTypes.RECORD_FIELD
    )

    protected fun parseOutputRecordSubfield(): Boolean {
        myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                NAME_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                TYPE_KEYWORD -> {
                    // CWLType | OutputRecordSchema | OutputEnumSchema | OutputArraySchema | string | array<CWLType | OutputRecordSchema | OutputEnumSchema | OutputArraySchema | string>
                    checkValue { parseSimpleType() }
                }
                OUTPUT_BINDING_KEYWORD -> {
                    checkValue(CwlElementTypes.OUTPUT_BINDING) { parsingContext.commandLineToolParser.parseCommandOutputBindingFields() }
                }
                DOC_KEYWORD, LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> false
            }
        }
    }

    fun parseEnumSchema(): Boolean = parseMap(
            parseStatement = {
                with(parsingContext.currentScope) {
                    when {
                        inInputs -> parseInputEnumSchemaField()
                        inOutputs -> parseOutputEnumSchemaField()
                        else -> false
                    }
                }
            }
            , blockType = CwlElementTypes.ENUM_SCHEMA
    )

    protected fun parseInputEnumSchemaField(): Boolean {
        myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                SYMBOLS_KEYWORD -> {
                    checkValue {
                        parseSimpleGeneralSequence(blockType = CwlElementTypes.SEQUENCE
                                , elementType = STRING)
                    }
                }
                TYPE_KEYWORD -> {
                    checkValue(parseValue =
                    {
                        checkMatches(CwlTokenTypes.ENUM_TYPE_KEYWORD, "enum expected")
                        checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    }
                    )
                }
                INPUT_BINDING_KEYWORD -> {
                    checkValue(CwlElementTypes.INPUT_BINDING)
                    { parsingContext.commandLineToolParser.parseInputBindingFields() }
                }
                LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> false
            }
        }
    }

    protected fun parseOutputEnumSchemaField(): Boolean {
        myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (myBuilder.tokenType) {
                SYMBOLS_KEYWORD -> {
                    checkValue {
                        parseSimpleGeneralSequence(blockType = CwlElementTypes.SEQUENCE
                                , elementType = STRING)
                    }
                }
                TYPE_KEYWORD -> {
                    checkValue(parseValue =
                    {
                        checkMatches(CwlTokenTypes.ENUM_TYPE_KEYWORD, "enum expected")
                        checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                    }
                    )
                }
                OUTPUT_BINDING_KEYWORD -> {
                    checkValue { parsingContext.commandLineToolParser.parseCommandOutputBindingFields() }
                }
                LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                else -> false
            }
        }
    }

    fun parseArraySchema(): Boolean = parseMap(
            parseStatement = {
                with(parsingContext.currentScope) {
                    when {
                        inInputs -> parseInputArraySchemaField()
                        inOutputs -> parseOutputArraySchemaField()
                        else -> false
                    }
                }
            }
            , blockType = CwlElementTypes.ARRAY_SCHEMA
    )

    protected fun parseInputArraySchemaField(): Boolean {
        myBuilder.tokenType?.let {
            with(CwlTokenTypes) {
                return when (it) {
                    ITEMS_KEYWORD -> checkValue { parseTypeValue() }
                    TYPE_KEYWORD -> {
                        checkValue(parseValue =
                        {
                            checkMatches(CwlTokenTypes.ARRAY_TYPE_KEYWORD, "array expected")
                            checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                        }
                        )
                    }
                    INPUT_BINDING_KEYWORD -> {
                        checkValue(CwlElementTypes.INPUT_BINDING)
                        { parsingContext.commandLineToolParser.parseInputBindingFields() }
                    }
                    LABEL_KEYWORD -> {
                        checkValue { parseStringValue() }
                    }
                    else -> false
                }
            }
        }
        return false
    }

    protected fun parseOutputArraySchemaField(): Boolean {
        myBuilder.tokenType?.let {
            with(CwlTokenTypes) {
                return when (it) {
                    ITEMS_KEYWORD -> checkValue { parseTypeValue() }
                    TYPE_KEYWORD -> {
                        checkValue(parseValue =
                        {
                            checkMatches(CwlTokenTypes.ARRAY_TYPE_KEYWORD, "array expected")
                            checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                        }
                        )
                    }
                    OUTPUT_BINDING_KEYWORD -> {
                        checkValue(CwlElementTypes.OUTPUT_BINDING) { parsingContext.commandLineToolParser.parseCommandOutputBindingFields() }
                    }
                    LABEL_KEYWORD -> {
                        checkValue { parseStringValue() }
                    }
                    else -> false
                }
            }
        }
        return false
    }

    // Workflow additional requirements
    protected fun parseRequirementHeader(requirement: IElementType): Boolean {
        // TODO think about usage of standard parseMap with lookAhead
        checkFullStatementWithSimpleKey(key = CwlTokenTypes.CLASS_KEYWORD) { matchToken(requirement) }
        return checkMatches(CwlTokenTypes.LINE_BREAK, message("PARSE.expected.line_break"))
    }

    protected fun parseWorkflowRequirementStatement(): Boolean {

        println("parseRequirement:: ${myBuilder.tokenType}")
        val tokenType = myBuilder.lookAhead(2) ?: return false
        println("lookahead $tokenType")
        with(CwlTokenTypes) {
            return when (tokenType) {
                SUBWORKFLOW_FEATURE_REQUIREMENT_KEYWORD,
                MULTIPLE_INPUT_FEATURE_REQUIREMENT_KEYWORD,
                STEP_INPUT_EXPRESSION_REQUIREMENT_KEYWORD,
                SCATTER_FEATURE_REQUIREMENT_KEYWORD -> {
                    parseRequirementHeader(tokenType)
                }
                else -> {
                    parseRequirementStatement()
                }
            }
        }
    }

    fun parseWorkflowRequirementsBlock(): Boolean =
            checkValue(CwlElementTypes.REQUIREMENTS_BLOCK) {
                parseSequence(parseElement = this::parseWorkflowRequirementStatement)
            }
}




