package org.cwlplugin.parser

import com.intellij.openapi.diagnostic.Logger
import org.cwlplugin.CwlBundle
import org.cwlplugin.psi.CwlElementTypes

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class WorkflowParser(context: ParsingContext) : CommandLineToolParser(context) {

    private val LOG = Logger.getInstance(RequirementsParser::class.java.name)

    fun parseWorkflowField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                REQUIREMENTS_KEYWORD -> {
                    parsingContext.requirementsParser.parseWorkflowRequirementsBlock()
                }
                INPUTS_KEYWORD -> {
                    checkStatement(CwlElementTypes.INPUTS) { parseInputs() }
                }
                OUTPUTS_KEYWORD -> {
                    checkStatement(CwlElementTypes.OUTPUTS) { parseOutputs() }
                }
                ID_KEYWORD, LABEL_KEYWORD, DOC_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                HINTS_KEYWORD -> {
                    checkValue { parseSequence { parseAny() } }
                }
                STEPS_KEYWORD -> {
                    parsingContext.pushScope(parsingContext.currentScope.withStep())
                    checkValue(CwlElementTypes.STEPS) { parseSteps() }.also { parsingContext.popScope() }
                }
                STDERR_KEYWORD, STDIN_KEYWORD, STDOUT_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                SUCCESS_CODES_KEYWORD ->
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.SUCCESS_CODES, CwlTokenTypes.INT) }
                TEMPORARY_FAIL_CODES_KEYWORD ->
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.TEMPORARY_FAIL_CODES, CwlTokenTypes.INT) }
                PERMANENT_FAIL_CODES_KEYWORD ->
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.PERMANENT_FAIL_CODES, CwlTokenTypes.INT) }
                else -> {
                    reportParseStatementError(myBuilder, firstToken)
                    return false
                }
            }
        }
    }

    override fun parseOutputParameterField()
            : Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        println("Workflow parse output")
        with(CwlTokenTypes) {
            return when (firstToken) {
                ID_KEYWORD, LABEL_KEYWORD -> checkValue { parseStringValue() }
                SECONDARY_FILES_KEYWORD -> {
                    checkStatement(CwlElementTypes.SECONDARY_FILES) { parsingContext.commandLineToolParser.parseSecondaryFiles() }
                }
                FORMAT_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parseExpression() } }
                }
                STREAMABLE_KEYWORD -> checkValue { parseBoolValue() }
                DOC_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parseSimpleGeneralSequence(CwlElementTypes.DOC, CwlTokenTypes.STRING) } }
                }
                OUTPUT_BINDING_KEYWORD -> {
                    checkValue {
                        parseCommandOutputBindingFields()
                    }
                }
                TYPE_KEYWORD -> {
                    // Check type
                    checkValue {
                        parseTypeValue()
                    }
                }
                OUTPUT_SOURCE_KEYWORD -> {
                    println("I'm in workflow outputs outputSource")
                    checkValue { parseStringValueOrSmth { parseSimpleGeneralSequence(elementType = CwlTokenTypes.STRING) } }
                }
                LINK_MERGE_KEYWORD -> {
                    checkValue(CwlElementTypes.LINK_MERGE_METHOD) { parseLinkMerge() }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken); return false
                }
            }
        }
    }


    private fun parseLinkMerge()
            : Boolean {
        with(CwlTokenTypes) {
            return if (atAnyOfTokens(MERGE_NESTED_KEYWORD, MERGE_FLATTENED_KEYWORD)) {
                nextToken()
                return checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
            } else false
        }
    }

    fun parseSteps(): Boolean {

        val tokenType = myBuilder.lookAhead(2) ?: return false
        return when (tokenType) {
        // array<WorkflowStep>
            CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX -> {

                parseSequence(parseElement = {
                    parseMap(parseStatement = { parseStepField() }
                            , blockType = CwlElementTypes.STEP
                    )
                })
            }
            CwlTokenTypes.STRING -> {
                // map<WorkflowStep.id, WorkflowStep>
                parseMap(parseStatement = this@WorkflowParser::parseStep
                )
            }
            else -> {
                println("lookAhead: $tokenType")
                nextToken()
                false
            }
        }
    }


    fun parseStep()
            : Boolean = if (!atToken(CwlTokenTypes.STRING)) false
    else checkStatement(parseValue = { parseMap(parseStatement = this::parseStepField) }
            , statementElement = CwlElementTypes.STEP
    )


    private fun parseStepField()
            : Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                ID_KEYWORD, LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                IN_KEYWORD -> {
                    parsingContext.pushScope(parsingContext.currentScope.withInputs())
                    checkValue(CwlElementTypes.STEP_IN) { parseInOrOutputs() }.also { parsingContext.popScope() }
                }
                OUT_KEYWORD -> {
                    parsingContext.pushScope(parsingContext.currentScope.withOutputs())
                    checkValue(CwlElementTypes.STEP_OUT) { parseStepOutput() }.also { parsingContext.popScope() }
                }
                RUN_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parsingContext.headerParser.parseTool() } }
                }
                DOC_KEYWORD -> {
                    // TODO: Is it true that only string is valid
                    checkValue { parseStringValue() }
                }
                REQUIREMENTS_KEYWORD -> {
                    parsingContext.requirementsParser.parseRequirementsBlock()
                    true
                }
                HINTS_KEYWORD -> {
                    checkValue { parseSequence { parseAny() } }
                }
                SCATTER_KEYWORD -> {
                    checkValue { parseStringOrArrayOfStrings() }
                }
                SCATTER_METHOD_KEYWORD -> {
                    checkValue { parseScatterMethod() }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken); return false
                }
            }
        }
    }

    private fun parseScatterMethod()
            : Boolean {
        with(CwlTokenTypes) {
            return if (matchAnyOfTokens(DOT_PRODUCT_KEYWORD, NESTED_CROSS_PRODUCT_KEYWORD, FLAT_CROSS_PRODUCT_KEYWORD)) {
                checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
            } else false
        }
    }

    fun parseWorkflowStepInputField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                DEFAULT_KEYWORD -> {
                    checkValue { parseAny() }
                }
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                LINK_MERGE_KEYWORD -> {
                    checkValue { parseLinkMerge() }
                }
                VALUE_FROM_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                SOURCE_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parseGeneralSequence { parseStringValue() } } }
                }
                else -> false
            }
        }
    }

    // array<string | WorkflowStepOutput>
    // TODO maybe make more restricted?
    fun parseStepOutput()
            : Boolean = parseGeneralSequence { parseStringValueOrSmth { parseWorkflowStepOutputField() } }

    fun parseWorkflowStepOutputField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                else -> false
            }
        }
    }
}