package org.cwlplugin.parser

import com.intellij.lang.PsiBuilder
import java.util.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class ParsingContext(val builder: PsiBuilder, val toolType: ToolType) {

    val commandLineToolParser = CommandLineToolParser(this)
    val requirementsParser = RequirementsParser(this)
    val workflowParser = WorkflowParser(this)
    val headerParser = HeaderParser(this)
    val expressionToolParser = ExpressionToolParser(this)
    val myScopes: Deque<ParsingScope> = ArrayDeque()
    val currentScope: ParsingScope
        get() = myScopes.peek()

    init {
        myScopes.push(emptyParsingScope())
    }

    fun popScope(): ParsingScope {
        println("$currentScope popped")
        return myScopes.pop()
    }

    fun pushScope(scope: ParsingScope) {
        println("$scope pushed")
        myScopes.push(scope)
    }

    fun emptyParsingScope(): ParsingScope = ParsingScope()

    override fun toString(): String {
        return myScopes.joinToString { "Scope $it: ${it.inMap} ${it.inSequence}" }
    }

}