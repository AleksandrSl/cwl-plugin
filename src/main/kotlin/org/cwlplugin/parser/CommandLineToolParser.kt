package org.cwlplugin.parser

import com.intellij.openapi.diagnostic.Logger
import org.cwlplugin.CwlBundle
import org.cwlplugin.psi.CwlElementTypes

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
open class CommandLineToolParser(context: ParsingContext) : Parsing(context) {

    private val LOG = Logger.getInstance(RequirementsParser::class.java.name)

    fun parseCommandLineToolField(): Boolean {

        val firstToken = myBuilder.tokenType ?: return true
        return with(CwlTokenTypes) {
            when (firstToken) {
                REQUIREMENTS_KEYWORD -> {
                    parsingContext.requirementsParser.parseRequirementsBlock()
                }
                INPUTS_KEYWORD -> {
                    checkValue(CwlElementTypes.INPUTS) { parseInputs() }
                }
                BASECOMMAND_KEYWORD -> {
                    checkValue(CwlElementTypes.BASE_COMMAND) { parseBaseCommand() }
                }
                OUTPUTS_KEYWORD -> {
                    checkValue(CwlElementTypes.OUTPUTS) { parseOutputs() }
                }
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                LABEL_KEYWORD, DOC_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                HINTS_KEYWORD -> {
                    checkValue { parseSequence { parseAny() } }
                }
                ARGUMENTS_KEYWORD -> {
                    checkValue(CwlElementTypes.ARGUMENTS) { parseArguments() }
                }
                STDERR_KEYWORD, STDIN_KEYWORD, STDOUT_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                SUCCESS_CODES_KEYWORD -> {
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.SUCCESS_CODES, CwlTokenTypes.INT) }
                }
                TEMPORARY_FAIL_CODES_KEYWORD -> {
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.TEMPORARY_FAIL_CODES, CwlTokenTypes.INT) }
                }
                PERMANENT_FAIL_CODES_KEYWORD -> {
                    checkValue { parseSimpleGeneralSequence(CwlElementTypes.PERMANENT_FAIL_CODES, CwlTokenTypes.INT) }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken)
                    false
                }
            }
        }
    }

    fun parseInputs(): Boolean {
        println("Current scope inInputs: ${parsingContext.currentScope.inInputs}")
        parsingContext.pushScope(parsingContext.currentScope.withInputs())
        println("New scope inInputs: ${parsingContext.currentScope.inInputs}")
        // array<CommandInputParameter> | map<CommandInputParameter.id, CommandInputParameter.type | CommandInputParameter>
        return parseInOrOutputs().also { parsingContext.popScope() }
    }

    fun parseOutputs(): Boolean {
        println("Current scope inOutputs: ${parsingContext.currentScope.inOutputs}")
        parsingContext.pushScope(parsingContext.currentScope.withOutputs())
        println("New scope inOutputs: ${parsingContext.currentScope.inOutputs}")
        return parseInOrOutputs().also { parsingContext.popScope(); println("One scoped pushed. Current scope inOutputs: ${parsingContext.currentScope.inOutputs}") }
    }

    fun parseInOrOutputs(): Boolean {
        // TODO get more info about rollbackTo() of marker, maybe it's better than lookAhead
        val tokenType = myBuilder.lookAhead(2) ?: return false
        return when (tokenType) {
        // array<Parameter>
            CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX -> {
                parseSequence(parseElement = { parseInOrOutputParameterFields() }
                )
            }
            CwlTokenTypes.STRING -> {
                // map<Parameter.id, Parameter | Parameter.type >
                // map<Parameter.id, Parameter | Parameter.source >
                parseInOrOutputParameters()
            }
            else -> {
                println("lookAhead: $tokenType")
                nextToken()
                false
            }
        }
    }

    private fun parseInOrOutputParameters(): Boolean = parseMap(parseStatement = this::parseInOrOutputParameter)

    private fun parseInOrOutputParameter(): Boolean =
            if (!atToken(CwlTokenTypes.STRING)) false
            else {
                // Not do double checkValue set parameterElement depending on scope
                val parameterElement = when {
                    parsingContext.currentScope.inInputs -> CwlElementTypes.INPUT_PARAMETER_STATEMENT
                    else -> CwlElementTypes.OUTPUT_PARAMETER
                }
                checkValue(parameterElement) {
                    if (atToken(CwlTokenTypes.LINE_BREAK)) parseInOrOutputParameterFields()
                    else {
                        with(parsingContext.currentScope) {
                            if (inStep && inInputs) {
                                parseStringValueOrSmth { parseSimpleFlowSequence(elementTypes = CwlTokenTypes.STRING) }
                            } else parseTypeValue()
                        }
                    }
                }
            }

    private fun parseInOrOutputParameterFields(): Boolean {
        with(parsingContext.currentScope) {
            return when {
                inStep -> {
                    when {
                        inOutputs ->
                            parsingContext.workflowParser.parseStepOutput()
                        inInputs -> {
                            println("I'm in step in!")
                            val tokenType = myBuilder.lookAhead(2) ?: return false
                            if (tokenType == CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX) {
                                parseSequence { parseStringValue() }
                            } else {
                                parseMap(parseStatement = { parsingContext.workflowParser.parseWorkflowStepInputField() }
                                        , blockType = CwlElementTypes.INPUT_PARAMETER_MAP
                                )
                            }
                        }
                        else -> false
                    }
                }
                inOutputs -> {
                    println("I'm in outputs scope")
                    parseMap(parseStatement = { parseOutputParameterField() }
                    )
                }
                inInputs -> {
                    println("I'm in inputs scope")
                    parseMap(parseStatement = { parseInputParameterField() }
                            , blockType = CwlElementTypes.INPUT_PARAMETER_MAP
                    )
                }
                else -> {
                    println("why I'm invoked without context?")
                    return false
                }
            }
        }
    }

    open fun parseInputParameterField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                DEFAULT_KEYWORD -> {
                    checkValue { parseAny() }
                }
                DOC_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parseSimpleGeneralSequence(CwlElementTypes.DOC, CwlTokenTypes.STRING) } }
                }
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                FORMAT_KEYWORD -> {
                    // TODO 	string | array<string> | Expression or it's a mistake
                    checkValue { parseStringValueOrSmth { parseSimpleGeneralSequence(elementType = CwlTokenTypes.STRING) } }
                }
                INPUT_BINDING_KEYWORD -> {
                    checkValue(CwlElementTypes.INPUT_BINDING) { parseInputBindingFields() }
                }
                SECONDARY_FILES_KEYWORD -> {
                    checkValue(CwlElementTypes.SECONDARY_FILES) { parseSecondaryFiles() }
                }
                STREAMABLE_KEYWORD -> {
                    checkValue { parseBoolValue() }
                }
                TYPE_KEYWORD -> {
                    checkValue { parseTypeValue() }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken)
                    false
                }
            }
        }
    }

    fun parseInputBindingFields(): Boolean {
        parseMap(parseStatement = this::parseInputBindingField)
        return true
    }

    fun parseInputBindingField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        println("$firstToken in inputBinding field")
        with(CwlTokenTypes) {
            when (firstToken) {
                VALUE_FROM_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                POSITION_KEYWORD -> {
                    checkValue(CwlElementTypes.POSITION) { parseIntValue() }
                }
                SEPARATE_KEYWORD -> {
                    checkValue(CwlElementTypes.SEPARATE) { parseBoolValue() }
                }
                SHELL_QUOTE_KEYWORD, LOAD_CONTENTS_KEYWORD -> {
                    checkValue { parseBoolValue() }
                }
                ITEM_SEPARATOR_KEYWORD -> {
                    checkValue(CwlElementTypes.ITEM_SEPARATOR) { parseStringValue() }
                }
                PREFIX_KEYWORD -> {
                    checkValue(CwlElementTypes.PREFIX) { parseStringValue() }
                }
                else -> {
                    println("parseInputBindingField")
                    reportParseStatementError(myBuilder, firstToken); return false
                }
            }
        }
        return true
    }


    fun parseSecondaryFiles(): Boolean = // I think that prohibiting multiline string is good
            parseEither(this::parseStringOrExpression, { parseGeneralSequence(parseElement = this::parseStringOrExpression) })

    private fun parseBaseCommand(): Boolean =
            // I think that prohibiting multiline string is good
            parseEither(CwlTokenTypes.STRING, { ->
                nextToken()
                checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
            },
                    { parseSimpleGeneralSequence(CwlElementTypes.BASE_COMMAND, CwlTokenTypes.STRING) }
            )

    // Maybe stdout and stdin []? prohibition should be done in code inspections, not here
    fun parseCommandOutputParameterType(): Boolean =
            when (myBuilder.tokenType) {
                CwlTokenTypes.STDOUT_KEYWORD, CwlTokenTypes.STDERR_KEYWORD -> {
                    nextToken()
                    checkMatches(CwlTokenTypes.LINE_BREAK, CwlBundle.message("PARSE.expected.line_break"))
                }
                else -> {
                    parseSimpleType()
                }
            }

    fun parseCommandInputParameterType(): Boolean =
            // Maybe stdout and stdin []? prohibition should be done in code inspections, not here
            parseSimpleType()

    open fun parseOutputParameterField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                ID_KEYWORD -> {
                    checkValue(CwlElementTypes.ID) { parseStringValue() }
                }
                LABEL_KEYWORD -> {
                    checkValue { parseStringValue() }
                }
                SECONDARY_FILES_KEYWORD -> {
                    checkValue(CwlElementTypes.SECONDARY_FILES) { parseSecondaryFiles() }
                }
                FORMAT_KEYWORD -> {
                    checkValue { parseStringOrExpression() }
                }
                STREAMABLE_KEYWORD -> {
                    checkValue { parseBoolValue() }
                }
                DOC_KEYWORD -> {
                    checkValue { parseStringValueOrSmth { parseSimpleGeneralSequence(CwlElementTypes.DOC, CwlTokenTypes.STRING) } }
                }

                OUTPUT_BINDING_KEYWORD -> {
                    checkValue { parseCommandOutputBindingFields() }
                }
                TYPE_KEYWORD -> {
                    checkValue { parseTypeValue() }
                }
                else -> {
                    reportParseStatementError(myBuilder, firstToken); return false
                }
            }
        }
    }

    fun parseCommandOutputBindingFields(): Boolean =
            parseMap(blockType = CwlElementTypes.OUTPUT_BINDING, parseStatement = { parseCommandOutputBindingField() })

    private fun parseCommandOutputBindingField(): Boolean {
        val firstToken = myBuilder.tokenType ?: return false
        with(CwlTokenTypes) {
            return when (firstToken) {
                GLOB_KEYWORD -> {
                    checkValue {
                        // string | Expression | array<string>
                        parseStringValueOrSmth {
                            parseEither(parseFirstElement = { parseExpression() },
                                    parseSecondElement = { parseSimpleGeneralSequence(CwlElementTypes.SEQUENCE, CwlTokenTypes.STRING) })
                        }
                    }
                }
                LOAD_CONTENTS_KEYWORD -> checkValue { parseBoolValue() }
                OUTPUT_EVAL_KEYWORD -> checkValue { parseStringOrExpression() }
                else -> {
                    reportParseStatementError(myBuilder, firstToken); return false
                }
            }
        }
    }

    fun parseStringOrArrayOfStrings(): Boolean =
            parseStringValueOrSmth { parseSimpleGeneralSequence(elementType = CwlTokenTypes.STRING) }

    private fun parseArguments(): Boolean =
            parseSequence(parseElement = this::parseArgument)

    private fun parseArgument(): Boolean =
            if (parseStringOrExpression()) true
            else {
                parseMap(
                        parseStatement = this::parseInputBindingField
                )
            }
}