package org.cwlplugin.parser

import com.intellij.psi.PsiElementVisitor
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlElementVisitor : PsiElementVisitor() {
    fun visitCwlElement(node: CwlElement) {
        visitElement(node)
    }

    fun visitCwlRequirementsBlock(node: CwlRequirements) {
        visitCwlElement(node)
    }

    fun visitCwlRequirementList(node: CwlRequirementList) {
        visitCwlElement(node)
    }

    fun visitCwlDockerRequirement(node: CwlDockerRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlSchemaDefRequirement(node: CwlSchemaDefRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlSoftwareRequirement(node: CwlSoftwareRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlInitialWorkDirRequirement(node: CwlInitialWorkDirRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlEnvVarRequirement(node: CwlEnvVarRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlShellCommandRequirement(node: CwlShellCommandRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlResourceRequirement(node: CwlResourceRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlInlineJavascriptRequirement(node: CwlInlineJavascriptRequirement) {
        visitCwlElement(node)
    }

    fun visitCwlVersion(node: CwlVersion) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}