package org.cwlplugin.findUsages

import com.intellij.lang.cacheBuilder.DefaultWordsScanner
import com.intellij.psi.tree.TokenSet
import org.cwlplugin.lexer.CwlIndentationLexer
import org.cwlplugin.parser.CwlTokenTypes

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlWordsScanner : DefaultWordsScanner(
        CwlIndentationLexer(), TokenSet.create(CwlTokenTypes.STRING),
        TokenSet.create(CwlTokenTypes.END_OF_LINE_COMMENT),
        TokenSet.create(CwlTokenTypes.MLSPART, CwlTokenTypes.EXPRESSION)
)
