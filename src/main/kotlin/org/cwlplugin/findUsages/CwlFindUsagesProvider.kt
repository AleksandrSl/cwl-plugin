package org.cwlplugin.findUsages

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */

import com.intellij.lang.cacheBuilder.WordsScanner
import com.intellij.lang.findUsages.FindUsagesProvider
import com.intellij.psi.PsiElement
import org.cwlplugin.psi.CwlInputParameter
import org.cwlplugin.psi.CwlNamedStatement
import org.cwlplugin.psi.CwlStatement

class CwlFindUsagesProvider : FindUsagesProvider {

    // XXX: must return new instance of WordScanner here, because it is not thread safe
    override fun getWordsScanner(): WordsScanner? = CwlWordsScanner()

    override fun canFindUsagesFor(psiElement: PsiElement): Boolean = psiElement is CwlNamedStatement

    override fun getHelpId(psiElement: PsiElement): String? = "Help me"

    override fun getType(element: PsiElement): String =
            when (element) {
                is CwlInputParameter -> "Input parameter"
                is CwlStatement -> "Statement"
                else -> "Something"
            }

    override fun getDescriptiveName(element: PsiElement): String =
            when (element) {
                is CwlInputParameter -> element.id + element.representation
                is CwlStatement -> element.keyText
                else -> "No descriptive name found"
            }

    override fun getNodeText(element: PsiElement, useFullName: Boolean): String =
            when (element) {
                is CwlStatement -> element.keyText
                else -> "No descriptive name found"
            }
}