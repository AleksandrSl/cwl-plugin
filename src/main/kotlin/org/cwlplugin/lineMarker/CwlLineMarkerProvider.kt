package org.cwlplugin.lineMarker

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiManager
import com.intellij.psi.search.FileTypeIndex
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.util.indexing.FileBasedIndex
import org.cwlplugin.CwlFileType
import org.cwlplugin.icons.CwlIcons
import org.cwlplugin.psi.CwlFile
import org.cwlplugin.psi.CwlStatement


/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlLineMarkerProvider : RelatedItemLineMarkerProvider() {
    override fun collectNavigationMarkers(element: PsiElement,
                                          result: MutableCollection<in RelatedItemLineMarkerInfo<*>>?) {
        if (element is CwlStatement) {
            if (element.keyText == "run") {

                val projectName = element.getProject().basePath
                val files = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, CwlFileType, GlobalSearchScope.allScope(element.project))
                files.filter { file -> file.canonicalPath == "$projectName/${element.valueText}" }
                        .mapNotNull { PsiManager.getInstance(element.project).findFile(it) }
                        .filterIsInstance<CwlFile>()
                        .map { it.firstChild }
                        .toList().also {
                    if (it.isNotEmpty()) {
                        val builder = NavigationGutterIconBuilder.create(CwlIcons.LINE_MARKER).setTargets(it).setTooltipText("Navigate to tool source file")
                        result!!.add(builder.createLineMarkerInfo(element))
                    }
                }
            }
        }
    }
}