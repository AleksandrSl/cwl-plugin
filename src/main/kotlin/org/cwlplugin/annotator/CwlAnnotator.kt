package org.cwlplugin.annotator

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import org.cwlplugin.CwlUtil
import org.cwlplugin.psi.CwlInputParameterStatement
import org.cwlplugin.psi.CwlNamedStatement
import org.cwlplugin.psi.CwlStep


/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlAnnotator : Annotator {
    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        if (element is CwlNamedStatement) {
            when {
                element is CwlInputParameterStatement -> {
                    println("Input parameter statement found !!!!!!!!!!")
                    element.getParentRecursive(CwlStep::class.java)?.let {
                        println("Found parent $it")
                        val toolFilePath = it.toolFilePath ?: return
                        val value = element.keyText
                        val project = element.getProject()
                        val properties = CwlUtil.findStatementsInFile(project, toolFilePath, value)
                        println("Propreties found ${properties.firstOrNull()}")
                        if (properties.size == 1) {
                            val range = TextRange(element.getTextRange().startOffset,
                                    element.getTextRange().startOffset + value.length)
                            val annotation = holder.createInfoAnnotation(range, null)
                            annotation.textAttributes = DefaultLanguageHighlighterColors.LINE_COMMENT
                        } else if (properties.isEmpty()) {
                            val range = TextRange(element.getTextRange().startOffset,
                                    element.getTextRange().startOffset + value.length)
                            holder.createErrorAnnotation(range, "Unresolved property")
                        }
                    }
                }
                else -> {
                    val value = if (element.keyText is String) element.keyText else null
                    if (value != null) {
                        val project = element.getProject()
                        val properties = CwlUtil.findStatements(project, value)
                        if (properties.size == 1) {
                            val range = TextRange(element.getTextRange().startOffset,
                                    element.getTextRange().startOffset + value.length)
                            val annotation = holder.createInfoAnnotation(range, null)
                            annotation.textAttributes = DefaultLanguageHighlighterColors.LINE_COMMENT
                        } else if (properties.isEmpty()) {
                            val range = TextRange(element.getTextRange().startOffset,
                                    element.getTextRange().startOffset + value.length)
                            holder.createErrorAnnotation(range, "Unresolved property")
                        }
                    }
                }
            }
        }
    }
}

tailrec fun <T : PsiElement> PsiElement.hasParent(parentClass: Class<T>): Boolean {
    return if (parentClass.isInstance(this.parent) || this.parent is PsiFile) {
        true
    } else this.parent.hasParent(parentClass)
}

tailrec fun <T : PsiElement> PsiElement.getParentRecursive(parentClass: Class<T>): T? {
    println("Parent ${this.parent}")
    return when {
        this.parent is PsiFile -> null
        parentClass.isInstance(this.parent) -> (this.parent as T).also { println("Parent found $it") }
        else -> this.parent.getParentRecursive(parentClass)
    }
}
