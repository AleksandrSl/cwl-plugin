package org.cwlplugin

import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiManager
import com.intellij.psi.search.FileTypeIndex
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.util.indexing.FileBasedIndex
import org.cwlplugin.psi.CwlFile
import org.cwlplugin.psi.CwlInputParameterStatement
import org.cwlplugin.psi.CwlNamedStatement


/**
 * Created by aleksandrsl on 08.05.17.
 */
object CwlUtil {

    fun findInputParameter(project: Project, relativeFilePath: String, identifier: String): CwlInputParameterStatement? =
            findStatementsInFile(project, relativeFilePath, identifier).filterIsInstance(CwlInputParameterStatement::class.java).firstOrNull()

    fun findInputParameters(project: Project, relativeFilePath: String): List<CwlInputParameterStatement> =
            findStatementsInFile(project = project, relativeFilePath = relativeFilePath).filterIsInstance(CwlInputParameterStatement::class.java)

    fun findStatements(project: Project): List<CwlNamedStatement> {

        val virtualFiles: Collection<VirtualFile> = FileBasedIndex.getInstance()
                .getContainingFiles<FileType, Void>(FileTypeIndex.NAME, CwlFileType,
                        GlobalSearchScope.allScope(project))

        return virtualFiles
                .mapNotNull { PsiManager.getInstance(project).findFile(it) as CwlFile? }
                .flatMap { PsiTreeUtil.findChildrenOfType(it, CwlNamedStatement::class.java).asIterable() }
                .filterIsInstance(CwlNamedStatement::class.java)
    }

    fun findStatements(project: Project, key: String): List<CwlNamedStatement> =
            findStatements(project).filter { it.keyText == key }

    fun findStatementsInFile(project: Project, relativeFilePath: String): List<CwlNamedStatement> {

        val virtualFiles: Collection<VirtualFile> = FileBasedIndex.getInstance()
                .getContainingFiles<FileType, Void>(FileTypeIndex.NAME, CwlFileType,
                        GlobalSearchScope.allScope(project))
                .onEach { println(it.canonicalPath) }
                .onEach { println(it.canonicalPath == "${project.basePath}/$relativeFilePath") }
                .filter { file -> file.canonicalPath == "${project.basePath}/$relativeFilePath" }

        return virtualFiles
                .mapNotNull { PsiManager.getInstance(project).findFile(it) as CwlFile? }
                .onEach { println("Search in file $it") }
                .flatMap { PsiTreeUtil.findChildrenOfType(it, CwlNamedStatement::class.java).asIterable() }
                .onEach { println("Find statement: $it") }
                .filterIsInstance(CwlNamedStatement::class.java)
    }

    fun findStatementsInFile(project: Project, relativeFilePath: String, key: String): List<CwlNamedStatement> =
            findStatementsInFile(project, relativeFilePath).filter { it.keyText == key }
}