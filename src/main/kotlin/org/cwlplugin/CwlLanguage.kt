package org.cwlplugin
import com.intellij.lang.Language

/**
 * Created by aleksandrsl on 06.05.17.
 */
object CwlLanguage: Language("cwl") {
    override fun getDisplayName(): String = "cwl"
}