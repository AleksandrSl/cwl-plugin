package org.cwlplugin

import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory


/**
 * Created by aleksandrsl on 06.05.17.
 */
class CwlFileTypeFactory: FileTypeFactory() {

    override fun createFileTypes(fileTypeConsumer: FileTypeConsumer): Unit {
            fileTypeConsumer.consume(CwlFileType, "cwl")
    }
}

