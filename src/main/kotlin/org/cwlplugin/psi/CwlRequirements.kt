package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlRequirements : CwlStatement

interface CwlDockerRequirement : CwlElement
interface CwlInlineJavascriptRequirement : CwlElement
interface CwlSchemaDefRequirement : CwlElement
interface CwlSoftwareRequirement : CwlElement
interface CwlInitialWorkDirRequirement : CwlElement
interface CwlEnvVarRequirement : CwlElement
interface CwlShellCommandRequirement : CwlElement
interface CwlResourceRequirement : CwlElement
interface CwlEnvironmentDef : CwlElement
interface CwlSoftwarePackage : CwlElement
