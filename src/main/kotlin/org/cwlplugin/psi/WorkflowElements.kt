package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlLinkMergeMethod : CwlElement

interface CwlStep : CwlNamedStatement {
    val toolFilePath: String?
}

interface CwlSteps : CwlStatement
interface CwlStepInput : CwlStatement
interface CwlStepOutput : CwlStatement
interface CwlScatterMethod : CwlElement