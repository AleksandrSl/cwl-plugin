package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlInputParameter : CwlElement {

    val type: CwlType?
    val isCommandLine: Boolean
    val position: Int?
    val prefix: String
    val separate: Boolean
    val id: String
    val representation: String
    val isInStep: Boolean
}

interface CwlInputParameterStatement : CwlNamedStatement, CwlInputParameter

interface CwlInputParameterMap : CwlMap, CwlInputParameter