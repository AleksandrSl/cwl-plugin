package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlOutputParameter : CwlStatement {
    val type: CwlType?
}