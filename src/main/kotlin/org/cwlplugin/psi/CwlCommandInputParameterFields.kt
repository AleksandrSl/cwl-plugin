package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlOutputBinding : CwlElement

// OutputBindingFields
interface CwlOutputEval : CwlElement

interface CwlGlob : CwlElement