package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.annotator.hasParent
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlInputParameterStatementImpl(node: ASTNode) : CwlNamedStatementImpl(node), CwlInputParameterStatement {

    override val type: CwlType?
        get() {
            // First clause is for simple format "id: type"
            return node.findChildByType(CwlElementTypes.CWL_TYPE)?.psi as CwlType? ?:
                    node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
                        (it.psi as CwlInputParameterMap).type
                    }
        }

    override val isCommandLine: Boolean
        get() = node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
            (it.psi as CwlInputParameterMap).isCommandLine
        } == true

    override val position: Int?
        get() = node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
            (it.psi as CwlInputParameterMap).position
        }

    override val prefix: String
        get() = node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
            (it.psi as CwlInputParameterMap).prefix
        } ?: ""

    override val separate: Boolean
        get() = node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
            (it.psi as CwlInputParameterMap).separate
        } != false

    override val id: String
        get() = keyText

    override val representation: String
        get() = node.findChildByType(CwlElementTypes.INPUT_PARAMETER_MAP)?.let {
            (it.psi as CwlInputParameterMap).representation
        } ?: ""

    override val isInStep: Boolean
        get() = node.psi.hasParent(CwlStepInput::class.java)
}
