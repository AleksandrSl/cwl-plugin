package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.parser.CwlElementVisitor
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlVersionImpl(node: ASTNode) : CwlElementImpl(node), CwlVersion {

    fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
        cwlVisitor.visitCwlVersion(this)
    }
}

class CwlInputsImpl(node: ASTNode) : CwlStatementImpl(node), CwlInputs {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlInputs(this)
//    }
}

class CwlOutputsImpl(node: ASTNode) : CwlStatementImpl(node), CwlOutputs {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlOutputs(this)
//    }
}

class CwlCommandLineToolClassImpl(node: ASTNode) : CwlElementImpl(node), CwlCommandLineToolClass {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlCommandLineToolClass(this)
//    }
}

class CwlWorkflowClassImpl(node: ASTNode) : CwlElementImpl(node), CwlWorkflowClass

class CwlExpressionToolClassImpl(node: ASTNode) : CwlElementImpl(node), CwlExpressionToolClass

class CwlBaseCommandImpl(node: ASTNode) : CwlElementImpl(node), CwlBaseCommand {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlBaseCommand(this)
//    }
}

class CwlIdImpl(node: ASTNode) : CwlStatementImpl(node), CwlId {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) = cwlVisitor.visitCwlId(this)
}

class CwlLabelImpl(node: ASTNode) : CwlElementImpl(node), CwlLabel {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlCwlLabel(this)
//    }
}

class CwlDocImpl(node: ASTNode) : CwlElementImpl(node), CwlDoc {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlDoc(this)
//    }
}

class CwlHintsImpl(node: ASTNode) : CwlElementImpl(node), CwlHints {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlHints(this)
//    }
}

class CwlArgumentsImpl(node: ASTNode) : CwlElementImpl(node), CwlArguments {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlArguments(this)
//    }
}

class CwlStderrImpl(node: ASTNode) : CwlElementImpl(node), CwlStderr {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlStderr(this)
//    }
}

class CwlStdinImpl(node: ASTNode) : CwlElementImpl(node), CwlStdin {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlStdin(this)
//    }
}

class CwlStdoutImpl(node: ASTNode) : CwlElementImpl(node), CwlStdout {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlStdout(this)
//    }
}

class CwlSuccessCodesImpl(node: ASTNode) : CwlElementImpl(node), CwlSuccessCodes {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlSuccessCodes(this)
//    }
}

class CwlTemporaryFailCodesImpl(node: ASTNode) : CwlElementImpl(node), CwlTemporaryFailCodes {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlTemporaryFailCodes(this)
//    }
}

class CwlPermanentFailCodesImpl(node: ASTNode) : CwlElementImpl(node), CwlPermanentFailCodes {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlPermanentFailCodes(this)
//    }
}

class CwlTypeImpl(node: ASTNode) : CwlElementImpl(node), CwlType {

    override fun typeAsString(): String =
            when {
            // Return types as joined string when there are plenty of them
                node.findChildByType(CwlElementTypes.SEQUENCE) != null -> {
                    text.trim().split(Regex("\\s")).map {
                        it.trim('-')
                    }.filter { it.isNotBlank() }.joinToString(separator = " | ")
                }
                node.findChildByType(CwlElementTypes.ARRAY_SCHEMA) != null -> {
                    "array"
                }
                node.findChildByType(CwlElementTypes.ENUM_SCHEMA) != null -> {
                    "enum"
                }
                node.findChildByType(CwlElementTypes.RECORD_SCHEMA) != null -> {
                    "record"
                }
                else -> text.trim()
            }

}