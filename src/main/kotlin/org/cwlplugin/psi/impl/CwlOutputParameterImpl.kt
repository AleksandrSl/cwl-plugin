package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import com.intellij.psi.tree.TokenSet
import org.cwlplugin.psi.CwlElementTypes
import org.cwlplugin.psi.CwlOutputParameter
import org.cwlplugin.psi.CwlType

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlOutputParameterImpl(node: ASTNode) : CwlStatementImpl(node), CwlOutputParameter {

    override val type: CwlType?
        get() {
            // First clause is for simple format "id: type"
            return node.findChildByType(CwlElementTypes.CWL_TYPE)?.psi as CwlType? ?:
                    node.findChildByType(CwlElementTypes.MAP)?.let {
                        it.getChildren(TokenSet.create(CwlElementTypes.STATEMENT))
                                .mapNotNull { it.findChildByType(CwlElementTypes.CWL_TYPE) }
                                .onEach { println("TEXT: ${it.text}") }
                                .map { (it.psi) }
                                .filterIsInstance(CwlType::class.java)
                                .firstOrNull()
                    }
        }
}