package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import com.intellij.psi.PsiElement
import com.intellij.psi.tree.TokenSet
import org.cwlplugin.parser.CwlTokenTypes
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlSequenceImpl(node: ASTNode) : CwlElementImpl(node), CwlSequence

class CwlFlowSequenceImpl(node: ASTNode) : CwlElementImpl(node), CwlFlowSequence
open class CwlMapImpl(node: ASTNode) : CwlElementImpl(node), CwlMap {
    override val meaningfulTextEndOffset: Int
        get() = node.text.trimEnd().length + node.startOffset
}

class CwlFileTypeImpl(node: ASTNode) : CwlElementImpl(node), CwlFileType
class CwlEnumSchemaImpl(node: ASTNode) : CwlElementImpl(node), CwlEnumSchema
class CwlArraySchemaImpl(node: ASTNode) : CwlElementImpl(node), CwlArraySchema
class CwlDirentImpl(node: ASTNode) : CwlElementImpl(node), CwlDirent
class CwlDirectoryImpl(node: ASTNode) : CwlElementImpl(node), CwlDirectory
class CwlRecordSchemaImpl(node: ASTNode) : CwlElementImpl(node), CwlRecordSchema
class CwlRecordFieldImpl(node: ASTNode) : CwlElementImpl(node), CwlRecordField

open class CwlStatementImpl(node: ASTNode) : CwlElementImpl(node), CwlStatement {

    override val colonStartOffset: Int
        get() = node.getChildren(TokenSet.create(CwlTokenTypes.COLON)).first().startOffset

    override val colonEndOffset: Int
        get() = colonStartOffset + 1

    // Statement is complex if it has map or sequence as value
    override val isComplex: Boolean
        get() = node.getChildren(TokenSet.create(CwlElementTypes.MAP, CwlElementTypes.SEQUENCE)).isNotEmpty()
//                .also { node.getChildren(TokenSet.ANY).forEach { println("Element type: ${it.elementType}") } }

    override val valueEndOffset: Int
        get() = node.text.trimEnd().length + node.startOffset

    override val valueStartOffset: Int
        get() = colonEndOffset

    override val valueText: String
        get() = node.text.substring(valueStartOffset - node.startOffset).trim()

    override val keyText: String
        get() = node.text.substring(0, colonStartOffset - node.startOffset).trim()

    override val key: PsiElement?
        get() = node.firstChildNode?.psi

}