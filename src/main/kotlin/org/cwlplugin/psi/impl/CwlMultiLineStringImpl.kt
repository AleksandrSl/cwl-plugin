package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.psi.CwlMultiLineString

/**
 * Created by aleksandrsl on 06.07.17.
 */
class CwlMultiLineStringImpl(node: ASTNode) : CwlElementImpl(node), CwlMultiLineString