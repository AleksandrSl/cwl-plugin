package org.cwlplugin.psi.impl

import org.cwlplugin.psi.CwlRequirementList

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlRequirementListImpl(node: com.intellij.lang.ASTNode) : CwlElementImpl(node), CwlRequirementList {

    fun acceptCwlVisitor(cwlVisitor: org.cwlplugin.parser.CwlElementVisitor) {
        cwlVisitor.visitCwlRequirementList(this)
    }
}