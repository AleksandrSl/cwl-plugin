package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry
import org.cwlplugin.factory.CwlElementFactory
import org.cwlplugin.psi.CwlNamedStatement


open class CwlNamedStatementImpl(node: ASTNode) : CwlStatementImpl(node), CwlNamedStatement {

    override fun getNameIdentifier(): PsiElement? = key.also { println("Get name identifier called") }

    override fun setName(name: String): PsiElement {
        val oldNode = node.firstChildNode
        println("Old key node $oldNode")
        oldNode?.let {
            val statement = CwlElementFactory.createStatement(this.project, name)
            val newKeyNode = statement.firstChild.node
            println("New key Node  $newKeyNode")
            this.node.replaceChild(oldNode, newKeyNode)
        }
        return this
    }

    override fun getName(): String? = key?.text

    // This is the core of evil. This method must be overridden for references to work
    override fun getReferences(): Array<PsiReference> {
        return ReferenceProvidersRegistry.getReferencesFromProviders(this)
    }
}
