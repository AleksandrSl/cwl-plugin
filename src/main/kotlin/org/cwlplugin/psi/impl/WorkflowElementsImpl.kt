package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import com.intellij.psi.util.PsiTreeUtil
import org.cwlplugin.psi.*


/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlLinkMergeMethodImpl(node: ASTNode) : CwlElementImpl(node), CwlLinkMergeMethod

class CwlStepImpl(node: ASTNode) : CwlNamedStatementImpl(node), CwlStep {
    override val toolFilePath: String?
        get() = PsiTreeUtil.findChildOfType(node.psi, CwlStatement::class.java)?.let {
            println("TEXT: ${it.text}")
            it.valueText
        }
}

class CwlStepsImpl(node: ASTNode) : CwlStatementImpl(node), CwlSteps
class CwlStepInImpl(node: ASTNode) : CwlStatementImpl(node), CwlStepInput
class CwlStepOutImpl(node: ASTNode) : CwlStatementImpl(node), CwlStepOutput
class CwlScatterMethodImpl(node: ASTNode) : CwlElementImpl(node), CwlScatterMethod

