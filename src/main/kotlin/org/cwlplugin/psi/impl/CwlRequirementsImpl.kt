package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.psi.*


/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlRequirementsImpl(node: ASTNode) : CwlStatementImpl(node), CwlRequirements {

//    val requirements: CwlRequirements
//        get() = childToPsiNotNull(CwlTokenTypes.REQUIREMENTS_TOKENS, 0)

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlRequirementsBlock(this)
//    }
}

class CwlDockerRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlDockerRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlDockerRequirement(this)
//    }
}

class CwlInlineJavascriptRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlInlineJavascriptRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlInlineJavascriptRequirement(this)
//    }
}

class CwlSchemaDefRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlSchemaDefRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlSchemaDefRequirement(this)
//    }
}

class CwlSoftwareRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlSoftwareRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlSoftwareRequirement(this)
//    }
}

class CwlInitialWorkDirRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlInitialWorkDirRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlInitialWorkDirRequirement(this)
//    }
}

class CwlEnvVarRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlEnvVarRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlEnvVarRequirement(this)
//    }
}

class CwlShellCommandRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlShellCommandRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlShellCommandRequirement(this)
//    }
}

class CwlResourceRequirementImpl(node: ASTNode) : CwlElementImpl(node), CwlResourceRequirement {

//    override fun acceptCwlVisitor(cwlVisitor: CwlElementVisitor) {
//        cwlVisitor.visitCwlResourceRequirement(this)
//    }
}

class CwlEnvironmentDefImpl(node: ASTNode) : CwlElementImpl(node), CwlEnvironmentDef
class CwlSoftwarePackageImpl(node: ASTNode) : CwlElementImpl(node), CwlSoftwarePackage
