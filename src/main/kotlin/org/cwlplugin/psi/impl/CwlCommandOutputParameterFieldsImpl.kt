package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.psi.CwlGlob
import org.cwlplugin.psi.CwlOutputBinding
import org.cwlplugin.psi.CwlOutputEval

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlOutputBindingImpl(node: ASTNode) : CwlElementImpl(node), CwlOutputBinding

// InputBindingFields
class CwlOutputEvalImpl(node: ASTNode) : CwlElementImpl(node), CwlOutputEval

class CwlGlobImpl(node: ASTNode) : CwlElementImpl(node), CwlGlob