package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlDefaultImpl(node: ASTNode) : CwlElementImpl(node), CwlDefault

class CwlFormatImpl(node: ASTNode) : CwlElementImpl(node), CwlFormat
class CwlInputBindingImpl(node: ASTNode) : CwlElementImpl(node), CwlInputBinding
class CwlSecondaryFilesImpl(node: ASTNode) : CwlElementImpl(node), CwlSecondaryFiles
class CwlStreamableImpl(node: ASTNode) : CwlElementImpl(node), CwlStreamable
//class CwlTypeImpl(node : ASTNode): CwlElementImpl(node), CwlType

// InputBindingFields
class CwlLoadContentsImpl(node: ASTNode) : CwlElementImpl(node), CwlLoadContents

class CwlValueFromImpl(node: ASTNode) : CwlElementImpl(node), CwlValueFrom
class CwlPositionImpl(node: ASTNode) : CwlStatementImpl(node), CwlPosition
class CwlSeparateImpl(node: ASTNode) : CwlStatementImpl(node), CwlSeparate
class CwlItemSeparatorImpl(node: ASTNode) : CwlElementImpl(node), CwlItemSeparator
class CwlPrefixImpl(node: ASTNode) : CwlStatementImpl(node), CwlPrefix
class CwlShellQuoteImpl(node: ASTNode) : CwlElementImpl(node), CwlShellQuote