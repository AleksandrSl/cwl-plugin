package org.cwlplugin.psi.impl

import com.intellij.lang.ASTNode
import com.intellij.psi.tree.TokenSet
import org.cwlplugin.annotator.hasParent
import org.cwlplugin.psi.*

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class CwlInputParameterMapImpl(node: ASTNode) : CwlMapImpl(node), CwlInputParameterMap {

    override val type: CwlType?
        get() = node.getChildren(TokenSet.create(CwlElementTypes.STATEMENT))
                .mapNotNull { it.findChildByType(CwlElementTypes.CWL_TYPE) }
                .onEach { println("TEXT: ${it.text}") }
                .map { (it.psi) }
                .filterIsInstance(CwlType::class.java)
                .firstOrNull()

    override val isCommandLine: Boolean
        get() = node.findChildByType(CwlElementTypes.INPUT_BINDING) != null


    override val position: Int?
        get() = node.getChildren(TokenSet.create(CwlElementTypes.INPUT_BINDING))
                .mapNotNull { it.findChildByType(CwlElementTypes.MAP) }
                .first()
                .findChildByType(CwlElementTypes.POSITION)?.let {
            (it.psi as CwlPosition).valueText.toIntOrNull()

        }

    override val prefix: String
        get() = node.getChildren(TokenSet.create(CwlElementTypes.INPUT_BINDING))
                .mapNotNull { it.findChildByType(CwlElementTypes.MAP) }
                .first()
                .findChildByType(CwlElementTypes.PREFIX)?.let {
            (it.psi as CwlPrefix).valueText.trim().trim('"')

        } ?: ""

    override val separate: Boolean
        get() = node.getChildren(TokenSet.create(CwlElementTypes.INPUT_BINDING))
                .mapNotNull { it.findChildByType(CwlElementTypes.MAP) }
                .first()
                .findChildByType(CwlElementTypes.SEPARATE)?.let {
            when ((it.psi as CwlSeparate).valueText.toLowerCase().trim()) {
                "true" -> true
                "false" -> false
                else -> true

            }
        } != false

    override val id: String
        get() = node.findChildByType(CwlElementTypes.ID)?.let {
            (it.psi as CwlId).valueText
        } ?: ""

    override val isInStep: Boolean
        get() = node.psi.hasParent(CwlStepInput::class.java)

    /**
     *  Short representation of input parameter
     *
     *  For command line parameters it includes prefix type and position
     *  in the next format: prefix type(position)
     *
     *  Prefix is separate from type if separate field is true, otherwise they are merged
     */
    override val representation: String
        get() {
            /*
            If id is specified explicitly add it.
            This is useful for input parameters in sequence,
            but looks a little bit strange when map `id: inputParameter` is used
             */
            val representationPrefix = if (id.isNotBlank()) "$id: " else " "

            return if (isCommandLine) {
                when {
                    prefix.isBlank() ->
                        representationPrefix.plus("${type?.typeAsString() ?: "..."}(${position ?: ""})")
                    else -> {
                        when {
                            separate -> representationPrefix.plus("$prefix ${type?.typeAsString() ?: "..."}(${position ?: ""})")
                            else -> representationPrefix.plus("$prefix${type?.typeAsString() ?: "..."}(${position ?: ""})")
                        }
                    }
                }
            } else {
                representationPrefix.plus(type?.typeAsString() ?: " ...")
            }
        }
}