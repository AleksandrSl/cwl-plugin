package org.cwlplugin.psi

import com.intellij.psi.PsiNameIdentifierOwner

interface CwlNamedStatement : PsiNameIdentifierOwner, CwlStatement