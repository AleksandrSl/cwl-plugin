package org.cwlplugin.psi

import com.intellij.psi.PsiElement

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlSequence : CwlElement

interface CwlMap : CwlElement {
    val meaningfulTextEndOffset: Int
}

interface CwlFlowSequence : CwlElement

interface CwlStatement : CwlElement {
    val key: PsiElement?
    val colonStartOffset: Int
    val colonEndOffset: Int
    val isComplex: Boolean
    val valueEndOffset: Int
    val valueStartOffset: Int
    val valueText: String
    val keyText: String
}