package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlDefault : CwlElement

interface CwlFormat : CwlElement
interface CwlInputBinding : CwlElement
interface CwlSecondaryFiles : CwlElement
interface CwlStreamable : CwlElement
//interface CwlType : CwlElement

// InputBindingFields
interface CwlLoadContents : CwlElement

interface CwlValueFrom : CwlElement
interface CwlPosition : CwlStatement
interface CwlSeparate : CwlStatement
interface CwlItemSeparator : CwlElement
interface CwlPrefix : CwlStatement
interface CwlShellQuote : CwlElement