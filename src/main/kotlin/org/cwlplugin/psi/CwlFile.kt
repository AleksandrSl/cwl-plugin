package org.cwlplugin.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider
import org.cwlplugin.CwlFileType
import org.cwlplugin.CwlLanguage
import org.cwlplugin.parser.ScopeOwner
import javax.swing.Icon

class CwlFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, CwlLanguage), CwlElement, ScopeOwner {

    override fun getFileType(): FileType = CwlFileType

    override fun toString(): String = "CWL File"

    override fun getIcon(flags: Int): Icon? = super.getIcon(flags)
}