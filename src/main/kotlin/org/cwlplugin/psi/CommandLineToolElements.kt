package org.cwlplugin.psi

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
interface CwlVersion : CwlElement

interface CwlInputs : CwlStatement
interface CwlOutputs : CwlStatement
interface CwlCommandLineToolClass : CwlElement
interface CwlBaseCommand : CwlElement
interface CwlId : CwlStatement
interface CwlLabel : CwlElement
interface CwlDoc : CwlElement
interface CwlHints : CwlElement
interface CwlArguments : CwlElement
interface CwlStderr : CwlElement
interface CwlStdin : CwlElement
interface CwlStdout : CwlElement
interface CwlSuccessCodes : CwlElement
interface CwlTemporaryFailCodes : CwlElement
interface CwlPermanentFailCodes : CwlElement
interface CwlType : CwlElement {
    fun typeAsString(): String
}

interface CwlWorkflowClass : CwlElement
interface CwlExpressionToolClass : CwlElement
interface CwlFileType : CwlElement // Not to clash with CwlFile
interface CwlEnumSchema : CwlElement
interface CwlArraySchema : CwlElement
interface CwlDirent : CwlElement
interface CwlDirectory : CwlElement
interface CwlRecordSchema : CwlElement
interface CwlRecordField : CwlElement