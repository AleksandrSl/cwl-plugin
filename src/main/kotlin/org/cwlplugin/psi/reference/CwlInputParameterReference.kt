package org.cwlplugin.psi.reference

import com.intellij.codeInsight.lookup.LookupElement
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.PsiReferenceBase
import org.cwlplugin.CwlUtil
import org.cwlplugin.annotator.getParentRecursive
import org.cwlplugin.icons.CwlIcons
import org.cwlplugin.psi.CwlInputParameterStatement
import org.cwlplugin.psi.CwlStep


class CwlInputParameterReference(element: CwlInputParameterStatement, textRange: TextRange) : PsiReferenceBase<CwlInputParameterStatement>(element, textRange), PsiReference {

    override fun resolve(): PsiElement? {

        return when {
            myElement.isInStep -> {
                myElement.getParentRecursive(CwlStep::class.java)?.let {
                    CwlUtil.findInputParameter(myElement.project, it.toolFilePath ?: return@let null, myElement.keyText)
                }
            }
            else -> null
        }
    }

    override fun handleElementRename(newElementName: String?): PsiElement {
        return myElement.setName(newElementName ?: "")
    }

    override fun getVariants(): Array<Any> {
        val variants = mutableListOf<LookupElement>()
        when {
            myElement.isInStep -> {
                myElement.getParentRecursive(CwlStep::class.java)?.let {
                    CwlUtil.findInputParameters(myElement.project, it.toolFilePath ?: return@let)
                            .mapTo(variants) {
                                LookupElementBuilder.create(it).
                                        withIcon(CwlIcons.FILE).
                                        withTypeText(it.containingFile.name)
                            }
                }
            }
        }
        return variants.toTypedArray()
    }
}