package org.cwlplugin.psi.reference

import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.openapi.util.TextRange
import com.intellij.psi.*
import org.cwlplugin.CwlUtil
import org.cwlplugin.icons.CwlIcons
import org.cwlplugin.psi.CwlNamedStatement

class CwlReference(element: PsiElement, textRange: TextRange) : PsiReferenceBase<PsiElement>(element, textRange), PsiPolyVariantReference {

    val key: String = element.text.substring(textRange.startOffset, textRange.endOffset)

    override fun multiResolve(incompleteCode: Boolean): Array<ResolveResult> {
        val project = myElement.project
        val statements: List<CwlNamedStatement> = CwlUtil.findStatements(project, key)
        return statements.map {
            PsiElementResolveResult(it)
        }.toTypedArray()
    }

    override fun resolve(): PsiElement? {

        val resolveResults = multiResolve(false)
        return if (resolveResults.size == 1) resolveResults[0].element else null
    }

    override fun getVariants(): Array<Any> {
        val project = myElement.project
        val statements: List<CwlNamedStatement> = CwlUtil.findStatements(project, key)
        return statements
                .filter {
                    it.key != null && it.key?.text?.isNotEmpty() == true
                }
                .map {
                    LookupElementBuilder.create(it).
                            withIcon(CwlIcons.FILE).
                            withTypeText(it.containingFile.name)
                }.toTypedArray()
    }

    override fun getValue(): String {
        return super.getValue()
    }
}