package org.cwlplugin.icons

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object CwlIcons {

    val FILE: Icon = IconLoader.getIcon("/icons/cwl-logo-20.png")

    val LINE_MARKER: Icon = IconLoader.getIcon("/icons/cwl-logo-30.png")
}
