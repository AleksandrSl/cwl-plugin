package org.cwlplugin.completion

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
object Keywords {

    val requirements = listOf("DockerRequirement", "InlineJavascriptRequirement",
            "SchemaDefRequirement", "SoftwareRequirement", "InitialWorkDirRequirement",
            "EnvVarRequirement", "ShellCommandRequirement", "ResourceRequirement")

    val workflowRequirements = listOf("SubworkflowFeatureRequirement",
            "ScatterFeatureRequirement", "MultipleInputFeatureRequirement", "StepInputExpressionRequirement").plus(requirements)

    val dockerRequirement = listOf("dockerPull", "dockerImport", "dockerFile", "dockerImageId",
            "dockerLoad", "dockerOutputDirectory")

    val resourceRequirement = listOf("coresMin", "coresMax", "ramMin", "ramMax", "tmpdirMin", "tmpdirMax", "outdirMin",
            "outdirMax")

    val envVarRequirement = listOf("envDef")

    val environmentDef = listOf("envName", "envValue")

    val initialWorkDirRequirement = listOf("listing")

    val softwareRequirement = listOf("packages")

    val softwarePackage = listOf("package", "version", "specs")

    val schemaDefRequirement = listOf("types")

    val inlineJavascriptRequirement = listOf("expressionLib")

    private val general = listOf("requirements", "cwlVersion", "inputs", "outputs", "class",
            "id", "label", "doc", "hints")

    val commandLineTool = listOf("baseCommand",
            "id", "label", "doc", "hints", "arguments", "stdin", "stderr", "stdout", "successCodes",
            "temporaryFailCodes", "permanentFailCodes").plus(general)

    val workflow = listOf("steps").plus(general)

    val expressionTool = listOf("expression").plus(general)

    val inputParameter = listOf("default", "doc", "label", "format", "inputBinding", "id", "secondaryFiles", "streamable",
            "type")

    val inputBinding = listOf("position", "prefix", "separate", "itemSeparator", "valueFrom",
            "shellQuote", "loadContents")

    val argument = inputBinding

    val outputParameter = listOf("label", "format", "id", "secondaryFiles", "type", "doc", "outputBinding", "streamable")

    val workflowOutputParameter = listOf("outputSource", "linkMerge").plus(outputParameter)

    val linkMergeMethod = listOf("merge_nested", "merge_flattened")

    val step = listOf("id", "in", "out", "run", "requirements", "hints", "label", "doc", "scatter", "scatterMethod")

    val stepInputParameter = listOf("id", "source", "linkMerge", "default", "valueFrom")

    val stepOutput = listOf("id")

    val scatterMethod = listOf("dotproduct", "nested_crossproduct", "flat_crossproduct")

    val outputBinding = listOf("glob", "loadContents", "outputEval")

    val types = listOf("boolean", "null", "int", "long", "float", "double", "string", "File", "Directory")

    val file = listOf("class", "location", "path", "basename", "dirname", "nameroot", "nameext", "checksum", "size",
            "secondaryFiles", "format", "contents")

    val directory = listOf("class", "location", "path", "basename", "listing")

    val dirent = listOf("entry", "entryName", "writable")

    val recordSchema = listOf("type", "label", "fields")

    val recordField = listOf("type", "name", "inputBinding", "outputBinding", "label", "doc")

    val enumSchema = listOf("type", "symbols", "label", "inputBinding", "outputBinding")

    val arraySchema = listOf("type", "items", "label", "inputBinding", "outputBinding")
}