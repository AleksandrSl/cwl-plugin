package org.cwlplugin.completion


import com.intellij.codeInsight.completion.CompletionContributor
import com.intellij.codeInsight.completion.CompletionType


/**
 * Created by aleksandrsl on 08.05.17.
 */
class CwlCompletionContributor : CompletionContributor() {

    init {

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.commandLineTool(),
                CwlKeywordCompletionProvider(Keywords.commandLineTool))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.workflow(),
                CwlKeywordCompletionProvider(Keywords.workflow))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.expressionTool(),
                CwlKeywordCompletionProvider(Keywords.expressionTool))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.requirements(),
                CwlKeywordCompletionProvider(Keywords.requirements))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.workflowRequirements(),
                CwlKeywordCompletionProvider(Keywords.workflowRequirements))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.inputBinding(),
                CwlKeywordCompletionProvider(Keywords.inputBinding))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.inputParameter(),
                CwlKeywordCompletionProvider(Keywords.inputParameter))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.dockerRequirement(),
                CwlKeywordCompletionProvider(Keywords.dockerRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.envVarRequirement(),
                CwlKeywordCompletionProvider(Keywords.envVarRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.resourceRequirement(),
                CwlKeywordCompletionProvider(Keywords.resourceRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.inlineJavascriptRequirement(),
                CwlKeywordCompletionProvider(Keywords.inlineJavascriptRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.initialWorkDirRequirement(),
                CwlKeywordCompletionProvider(Keywords.initialWorkDirRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.schemaDefRequirement(),
                CwlKeywordCompletionProvider(Keywords.schemaDefRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.softwareRequirement(),
                CwlKeywordCompletionProvider(Keywords.softwareRequirement))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.outputBinding(),
                CwlKeywordCompletionProvider(Keywords.outputBinding))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.outputParameter(),
                CwlKeywordCompletionProvider(Keywords.outputParameter))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.linkMergeMethod(),
                CwlKeywordCompletionProvider(Keywords.linkMergeMethod))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.step(),
                CwlKeywordCompletionProvider(Keywords.step))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.stepInputParameter(),
                CwlKeywordCompletionProvider(Keywords.stepInputParameter))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.stepOutput(),
                CwlKeywordCompletionProvider(Keywords.stepOutput))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.scatterMethod(),
                CwlKeywordCompletionProvider(Keywords.scatterMethod))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.workflowOutputParameter(),
                CwlKeywordCompletionProvider(Keywords.workflowOutputParameter))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.file(),
                CwlKeywordCompletionProvider(Keywords.file))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.directory(),
                CwlKeywordCompletionProvider(Keywords.directory))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.dirent(),
                CwlKeywordCompletionProvider(Keywords.dirent))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.recordField(),
                CwlKeywordCompletionProvider(Keywords.recordField))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.recordSchema(),
                CwlKeywordCompletionProvider(Keywords.recordSchema))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.arraySchema(),
                CwlKeywordCompletionProvider(Keywords.arraySchema))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.enumSchema(),
                CwlKeywordCompletionProvider(Keywords.enumSchema))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.environmentDef(),
                CwlKeywordCompletionProvider(Keywords.environmentDef))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.softwarePackage(),
                CwlKeywordCompletionProvider(Keywords.softwarePackage))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.types(),
                CwlKeywordCompletionProvider(Keywords.types))

        extend(CompletionType.BASIC, CwlKeywordCompletionProvider.argument(),
                CwlKeywordCompletionProvider(Keywords.argument))
    }
}

