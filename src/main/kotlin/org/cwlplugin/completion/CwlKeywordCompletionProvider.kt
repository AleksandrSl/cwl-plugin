package org.cwlplugin.completion

import com.intellij.codeInsight.completion.*
import com.intellij.codeInsight.lookup.LookupElement
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.patterns.PlatformPatterns
import com.intellij.patterns.PsiElementPattern
import com.intellij.psi.PsiElement
import com.intellij.util.ProcessingContext
import org.cwlplugin.parser.CwlTokenTypes
import org.cwlplugin.psi.*

/**
 * Created by aleksandrsl on 14.06.17.
 */
class CwlKeywordCompletionProvider(private val keywords: List<String>)
    : CompletionProvider<CompletionParameters>() {

    override fun addCompletions(parameters: CompletionParameters, context: ProcessingContext?, result: CompletionResultSet) {
        keywords.forEach { keyword ->
            var builder = LookupElementBuilder.create(keyword)
            PREFIXES.plus(SUFFIXES).filter { keyword in it.second }
                    .firstOrNull()
                    ?.let { builder = builder.withInsertHandler(it.first) }
            result.addElement(builder)
        }
    }

    companion object {

        fun commandLineTool(): PsiElementPattern.Capture<PsiElement> =
                psiElement<PsiElement>()
                        .inside(true, psiElement<CwlCommandLineToolClass>())
                        .withParent(psiElement<CwlCommandLineToolClass>())

        fun workflow(): PsiElementPattern.Capture<PsiElement> =
                psiElement<PsiElement>()
                        .inside(true, psiElement<CwlWorkflowClass>())
                        .withParent(psiElement<CwlWorkflowClass>())

        fun expressionTool(): PsiElementPattern.Capture<PsiElement> =
                psiElement<PsiElement>()
                        .inside(true, psiElement<CwlExpressionToolClass>())
                        .withParent(psiElement<CwlExpressionToolClass>())

        fun requirements(): PsiElementPattern.Capture<PsiElement> =
                psiElement<PsiElement>()
                        .inside(psiElement<CwlRequirements>())
                        .withAncestor(2, psiElement<CwlRequirements>())

        // TODO What to do with commandLineTool inside workflow?
        fun workflowRequirements(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlRequirements>())
                .inside(true, psiElement<CwlWorkflowClass>())
                .withAncestor(2, psiElement<CwlRequirements>())

        fun resourceRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlResourceRequirement>())

        fun envVarRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlEnvVarRequirement>())

        fun environmentDef(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlEnvironmentDef>())

        fun dockerRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlDockerRequirement>())

        fun initialWorkDirRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlInitialWorkDirRequirement>())
                .withParent(psiElement<CwlInitialWorkDirRequirement>())

        fun softwareRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlSoftwareRequirement>())
                .withParent(psiElement<CwlSoftwareRequirement>())

        fun softwarePackage(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlSoftwarePackage>())

        fun schemaDefRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlSchemaDefRequirement>())
                .withParent(psiElement<CwlSchemaDefRequirement>())

        fun inlineJavascriptRequirement(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlInlineJavascriptRequirement>())

        fun inputBinding(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlInputBinding>())

        fun inputParameter(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlInputParameter>()).andNot(psiElement<PsiElement>()
                .inside(true, psiElement<CwlStep>()))

        fun outputParameter(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlOutputParameter>())

        fun workflowOutputParameter(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlOutputParameter>()).inside(psiElement<CwlWorkflowClass>())

        fun outputBinding(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(psiElement<CwlOutputBinding>())

        fun types(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .afterLeaf(psiElement<PsiElement>().afterLeaf(PlatformPatterns.psiElement(CwlTokenTypes.TYPE_KEYWORD)))

        fun linkMergeMethod(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlLinkMergeMethod>())

        fun step(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlStep>()).withAncestor(2, psiElement<CwlStep>())

        fun stepInputParameter(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .withParent(psiElement<CwlInputParameter>())
                .inside(true, psiElement<CwlStepInput>())

        fun stepOutput(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlStepOutput>())

        fun scatterMethod(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlScatterMethod>())

        fun directory(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlDirectory>())

        fun dirent(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlDirent>())

        fun file(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlFileType>())

        fun recordSchema(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlRecordSchema>())

        fun recordField(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlRecordField>())

        fun enumSchema(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlEnumSchema>())

        fun arraySchema(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlArraySchema>())

        fun argument(): PsiElementPattern.Capture<PsiElement> = psiElement<PsiElement>()
                .inside(true, psiElement<CwlSequence>()).inside(psiElement<CwlArguments>())

        private val PREFIXES = listOf(
                AddPrefix("class: ") to Keywords.workflowRequirements
        )

        private val SUFFIXES = listOf(
                AddSuffix(": ") to Keywords.commandLineTool
                        .plus(Keywords.inputBinding)
                        .plus(Keywords.inputParameter)
                        .plus(Keywords.outputParameter)
                        .plus(Keywords.dockerRequirement)
        )

        /**
         * From Rust plugin
         */
        inline fun <reified I : PsiElement> psiElement(): PsiElementPattern.Capture<I> =
                PlatformPatterns.psiElement(I::class.java)
    }

    private class AddSuffix(val suffix: String) : InsertHandler<LookupElement> {
        override fun handleInsert(context: InsertionContext, item: LookupElement) {
            context.document.insertString(context.selectionEndOffset, suffix)
            EditorModificationUtil.moveCaretRelatively(context.editor, suffix.length)
        }
    }

    private class AddPrefix(val prefix: String) : InsertHandler<LookupElement> {
        override fun handleInsert(context: InsertionContext, item: LookupElement) {
            context.document.insertString(context.startOffset, prefix)
        }
    }
}

