package org.cwlplugin.completion

import com.intellij.codeInsight.completion.CompletionParameters
import com.intellij.codeInsight.completion.CompletionProvider
import com.intellij.codeInsight.completion.CompletionResultSet
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.util.ProcessingContext

/**
 * @author Aleksandr Slepchenkov [aslepchenkov@parseq.pro](mailto:aslepchenkov@parseq.pro)
 */
class SimpleProvider(val items: List<String>) : CompletionProvider<CompletionParameters>() {

    override fun addCompletions(parameters: CompletionParameters, context: ProcessingContext?, result: CompletionResultSet): Unit {
            println("Add completion is used")
            items.map { LookupElementBuilder.create(it).withCaseSensitivity(false) }
                    .onEach{ println("Element in lookup list: $it") }
                    .forEach(result::addElement)

    }

}