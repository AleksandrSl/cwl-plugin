package org.cwlplugin.factory

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.util.PsiTreeUtil
import org.cwlplugin.CwlFileType
import org.cwlplugin.psi.CwlFile
import org.cwlplugin.psi.CwlNamedStatement
import org.cwlplugin.psi.CwlStatement

object CwlElementFactory {

    fun createStatement(project: Project, key: String, value: String = "int"): CwlStatement {
        val file: CwlFile = createFile(project, """$commandLineTool  $key:
            |    type: $value""".trimMargin())
        // Find first named statement
        return PsiTreeUtil.findChildrenOfType(file, CwlNamedStatement::class.java).first() as CwlStatement
    }

    fun createCRLF(project: Project): PsiElement =
            createFile(project, "\n").firstChild

    fun createFile(project: Project, text: String): CwlFile = PsiFileFactory.getInstance(project).
            createFileFromText("dummy.cwl", CwlFileType, text) as CwlFile

    private val commandLineTool =
            """cwlVersion: v1.0
class: CommandLineTool

inputs:

"""
}