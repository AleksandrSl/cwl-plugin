package org.cwlplugin.folding

import com.intellij.lang.ASTNode
import com.intellij.lang.folding.FoldingBuilderEx
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.FoldingGroup
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil
import org.cwlplugin.completion.startOffset
import org.cwlplugin.psi.*
import org.cwlplugin.psi.impl.*

class CwlFoldingBuilder : FoldingBuilderEx() {

    override fun buildFoldRegions(root: PsiElement, document: Document, quick: Boolean)
            : Array<FoldingDescriptor> {

        val descriptors: MutableList<FoldingDescriptor> = ArrayList()
        val inputParameters: Collection<CwlInputParameterMap> =
                PsiTreeUtil.findChildrenOfType(root, CwlInputParameterMapImpl::class.java)
        val outputParameters: Collection<CwlOutputParameter> =
                PsiTreeUtil.findChildrenOfType(root, CwlOutputParameterImpl::class.java)
        val steps: Collection<CwlStep> =
                PsiTreeUtil.findChildrenOfType(root, CwlStepImpl::class.java)
        val inputs: Collection<CwlInputs> =
                PsiTreeUtil.findChildrenOfType(root, CwlInputsImpl::class.java)
        val outputs: Collection<CwlOutputs> =
                PsiTreeUtil.findChildrenOfType(root, CwlOutputsImpl::class.java)
        val requirements: Collection<CwlRequirements> =
                PsiTreeUtil.findChildrenOfType(root, CwlRequirementsImpl::class.java)
        val allSteps: Collection<CwlSteps> =
                PsiTreeUtil.findChildrenOfType(root, CwlStepsImpl::class.java)

        inputParameters
                .mapTo(descriptors) {
                    object : FoldingDescriptor(it.node,
                            TextRange(it.startOffset,
                                    it.meaningfulTextEndOffset),
                            FoldingGroup.newGroup("input parameters")) {
                        override fun getPlaceholderText(): String = it.representation
                    }
                }

        outputParameters
                .filter { it.isComplex }
                .mapTo(descriptors) {
                    object : FoldingDescriptor(it.node,
                            TextRange(it.colonEndOffset,
                                    it.valueEndOffset),
                            FoldingGroup.newGroup("output parameters")) {
                        override fun getPlaceholderText(): String = " ${it.type?.typeAsString() ?: " ..."}"
                    }
                }

        // TODO fold all complex maps??
        steps.plus(inputs).plus(outputs).plus(requirements).plus(allSteps)
                .filter { it.isComplex }
                .mapTo(descriptors) {
                    object : FoldingDescriptor(it.node,
                            TextRange(it.colonEndOffset,
                                    it.valueEndOffset),
                            FoldingGroup.newGroup("major statements")) {
                        override fun getPlaceholderText(): String = " ..."
                    }
                }

        return descriptors.toTypedArray()
    }

    override fun getPlaceholderText(node: ASTNode): String = " ..."

    override fun isCollapsedByDefault(node: ASTNode): Boolean = false
}
