# IntelliJ Idea CWL plugin

## Archived

This project is archived because I think IDEA is too "complex" for CWL. 
I think something like rabix-composer is a better tool
and I don't have time to work on this plugin

## Description

Plugin for IntelliJ IDEA to work with
[Common Workflow Language](http://www.commonwl.org/v1.0/index.html) files

## Installation

* Copy the `cwl-plugin.jar` to the .IntelliJIDEAx0\config\plugins folder,
and then restart your IDE so the changes may take effect.
To know how to locate your plugins directory, refer to [IDE Settings,
Caches, Logs, and Plugins](http://www.jetbrains.org/intellij/sdk/docs/basics/settings_caches_logs.html).

* In the main menu, select File | Settings to open the Settings dialog box.
* In the Settings dialog box, under IDE Settings, click Plugins.
* In the Plugins area, open the Installed tab, and then select the check-box next to your plugin name.
* When finished, click OK to close the Settings dialog box.
* Restart the IDE so that your changes take effect.

## Supported features

* Syntax check for CommandLineTool, ExpressionTool, Workflow
* Autocompletion
* Syntax highlighting. Can be tuned in `File > Settings > Editor > Color&Fonts > CWL`
* Line/block commenting by `Ctrl + /`
* Reference resolution and find usages by `Ctrl + B`, `Alt + F7`, `Ctrl + Left mouse button`
* Code folding
* Parameters refactoring `Shift + F6`
* Jump to source file from workflow step

For more detailed description see wiki

File with supported syntax can be found in `example-project` folder

********************************************************************************
## Contribution

### Requirements for development

To compile you will need:

* IDEA 2017.1.2
* Kotlin plugin
* JDK 8
* IntelliJ IDEA Plugin SDK

### Guides and Tutorials

[JetBrains Custom Language Support](http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/code_style_settings.html)

[JetBrains GramarKit Tutorial](https://github.com/JetBrains/Grammar-Kit/blob/master/TUTORIAL.md)

[JFlex Documentation](http://jflex.de/manual.html#SECTION00053000000000000000)

[Haskell Plugin](https://github.com/atsky/haskell-idea-plugin/tree/f72db3f1fae672e3ec171074ebb6b59509e9b88b)

[Rust Plugin](https://github.com/intellij-rust/intellij-rust/tree/bda24c7dd88bb10ca2f1a4632154d1e5774bfd6b)

[D Plugin](https://github.com/intellij-dlanguage/intellij-dlanguage)

[Kotlin Plugin](https://github.com/JetBrains/kotlin/blob/4b23c50bf81447257c9d5c4619feb524f0ed9744/compiler/frontend/src/org/jetbrains/kotlin/lexer/Kotlin.flex)

********************************************************************************

## Authors

Slepchenkov Aleksandr <Sl.aleksandr28@gmail.com>
