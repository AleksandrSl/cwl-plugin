package org.cwlplugin.lexer;

import com.intellij.lexer.FlexLexer;

interface CwlFlexLexer extends FlexLexer {

    void yybeginMultiLineString();

    default boolean isInMultiLineStringState() {
        return yystate() == getMultilineStringState();
    }

    int getMultilineStringState();
}