package org.cwlplugin.lexer;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;
import org.cwlplugin.parser.CwlTokenTypes;

%%

%class _CwlLexer
%{
  public _CwlLexer() {
      this((java.io.Reader)null);
  }

  @Override
  public void yybeginMultiLineString() {
      yybegin(getMultilineStringState());
  }

  @Override
  public int getMultilineStringState() {
      return MULTILINE_STRING;
  }
%}
%implements CwlFlexLexer
%unicode
%function advance
%type IElementType
%eof{
    return;
%eof}

/* To maintain portability, tab characters must not be used in indentation, since different systems treat tabs differently. Note that
/   most modern editors may be configured so that pressing the tab key results in the insertion of an appropriate number of spaces.
/   The amount of indentation is a presentation detail and must not be used to convey content information.
/  [63] s-indent(n) ::= s-space × n
*/

/*
A block style construct is terminated when encountering a line which is less indented than the construct. The productions use the
notation “s-indent(<n)” and “s-indent(≤n)” to express this.
[64] s-indent(<n) ::= s-space × m /* Where m < n */
[65] s-indent(≤n) ::= s-space × m /* Where m ≤ n */
 */

LineTerminator = \R
// Comment can be the last line of the file, without line terminator
Comment = {EndOfLineComment}
EndOfLineComment = #.*
DecIntegerLiteral = 0 | [1-9][0-9]*
DoubleQuotedString = \"[^\"\r\n]*\"
SingleQuotedString = '[^\"\r\n]*'
SimpleString = {DoubleQuotedString} | {SingleQuotedString}

// # can be inside string, but in this case aaa #aaaa will be a string also, not a string and comment
// Bless the person who allowed strings without quotation marks!
// Permamently or maybe constantly restrict bare string value
StartSymbols = [A-Za-z._/\^*]
MiddleSymbols = [^\n\r\(? :#\[\]\),\"]
TrailingSymbols = [^\n\r\(: #?\[\]\),\"]
// Now - and : can be in string but only without trailing space and # without preceding space
BareString = (({StartSymbols} | -{TrailingSymbols})({MiddleSymbols} | " "*{TrailingSymbols} | :{TrailingSymbols} | {TrailingSymbols}#)*)

String = {BareString} | {SimpleString}
Expression = \$\(.*\) | \$\{.*\}
Boolean = True | False | false | true

%state MULTILINE_STRING

%%
    <YYINITIAL> {

        [\ ]                               { return CwlTokenTypes.SPACE; }
        [\t]                               { return CwlTokenTypes.TAB; }
        [\f]                               { return CwlTokenTypes.FORMFEED; }
        {LineTerminator}                   { return CwlTokenTypes.LINE_BREAK; }
        "?"                                { return CwlTokenTypes.QUESTION_MARK; }
        "|"                                { return CwlTokenTypes.PIPE; }
        ">"                                { return CwlTokenTypes.GT; }
        - / (" " | {LineTerminator})       { return CwlTokenTypes.SEQUENCE_ELEMENT_PREFIX; }
        {Comment}                          { return CwlTokenTypes.END_OF_LINE_COMMENT; }
        {DecIntegerLiteral}                { return CwlTokenTypes.INT; }
        "array"                            { return CwlTokenTypes.ARRAY_TYPE_KEYWORD; }
        "arguments"                        { return CwlTokenTypes.ARGUMENTS_KEYWORD; }
        "baseCommand"                      { return CwlTokenTypes.BASECOMMAND_KEYWORD; }
        "basename"                         { return CwlTokenTypes.BASENAME_KEYWORD; }
        {Boolean}                          { return CwlTokenTypes.BOOLEAN; }
        "boolean"                          { return CwlTokenTypes.BOOLEAN_TYPE_KEYWORD; }
        "checksum"                         { return CwlTokenTypes.CHECKSUM_KEYWORD; }
        "class"                            { return CwlTokenTypes.CLASS_KEYWORD; }
        ":" / (" " | {LineTerminator})     { return CwlTokenTypes.COLON; }
        ","                                { return CwlTokenTypes.COMMA; }
        "CommandLineTool"                  { return CwlTokenTypes.COMMAND_LINE_TOOL_KEYWORD; }
        "contents"                         { return CwlTokenTypes.CONTENTS_KEYWORD; }
        "coresMax"                         { return CwlTokenTypes.CORES_MAX_KEYWORD; }
        "coresMin"                         { return CwlTokenTypes.CORES_MIN_KEYWORD; }
        "cwlVersion"                       { return CwlTokenTypes.CWL_VERSION; }
        "default"                          { return CwlTokenTypes.DEFAULT_KEYWORD; }
        "Directory"                        { return CwlTokenTypes.DIRECTORY_TYPE_KEYWORD; }
        "dirname"                          { return CwlTokenTypes.DIRNAME_KEYWORD; }
        "doc"                              { return CwlTokenTypes.DOC_KEYWORD; }
        "dockerFile"                       { return CwlTokenTypes.DOCKER_FILE_KEYWORD; }
        "dockerImageId"                    { return CwlTokenTypes.DOCKER_IMAGE_ID_KEYWORD; }
        "dockerImport"                     { return CwlTokenTypes.DOCKER_IMPORT_KEYWORD; }
        "dockerLoad"                       { return CwlTokenTypes.DOCKER_LOAD_KEYWORD; }
        "dockerOutputDirectory"            { return CwlTokenTypes.DOCKER_OUTPUT_DIRECTORY_KEYWORD; }
        "dockerPull"                       { return CwlTokenTypes.DOCKER_PULL_KEYWORD; }
        "DockerRequirement"                { return CwlTokenTypes.DOCKER_REQUIREMENT_KEYWORD; }
        "dotproduct"                       { return CwlTokenTypes.DOT_PRODUCT_KEYWORD; }
        "double"                           { return CwlTokenTypes.DOUBLE_TYPE_KEYWORD; }
        "entry"                            { return CwlTokenTypes.ENTRY_KEYWORD; }
        "entryname"                        { return CwlTokenTypes.ENTRYNAME_KEYWORD; }
        "enum"                             { return CwlTokenTypes.ENUM_TYPE_KEYWORD; }
        "expressionLib"                    { return CwlTokenTypes.EXPRESSION_LIB_KEYWORD; }
        "envDef"                           { return CwlTokenTypes.ENV_DEF_KEYWORD; }
        "envName"                          { return CwlTokenTypes.ENV_NAME_KEYWORD; }
        "envValue"                         { return CwlTokenTypes.ENV_VALUE_KEYWORD; }
        "EnvVarRequirement"                { return CwlTokenTypes.ENV_VAR_REQUIREMENT_KEYWORD; }
        {Expression}                       { return CwlTokenTypes.EXPRESSION; }
        "expression"                       { return CwlTokenTypes.EXPRESSION_KEYWORD; }
        "ExpressionTool"                   { return CwlTokenTypes.EXPRESSION_TOOL_KEYWORD; }
        "fields"                           { return CwlTokenTypes.FIELDS_KEYWORD; }
        "flat_crossproduct"                { return CwlTokenTypes.FLAT_CROSS_PRODUCT_KEYWORD; }
        "File"                             { return CwlTokenTypes.FILE_TYPE_KEYWORD; }
        "float"                            { return CwlTokenTypes.FLOAT_TYPE_KEYWORD; }
        "format"                           { return CwlTokenTypes.FORMAT_KEYWORD; }
        "glob"                             { return CwlTokenTypes.GLOB_KEYWORD; }
        "hints"                            { return CwlTokenTypes.HINTS_KEYWORD; }
        "id"                               { return CwlTokenTypes.ID_KEYWORD; }
        "$import"                          { return CwlTokenTypes.IMPORT_KEYWORD; }
        "in"                               { return CwlTokenTypes.IN_KEYWORD; }
        "$include"                         { return CwlTokenTypes.INCLUDE_KEYWORD; }
        "InitialWorkDirRequirement"        { return CwlTokenTypes.INITIAL_WORKDIR_REQUIREMENT_KEYWORD; }
        "InlineJavascriptRequirement"      { return CwlTokenTypes.INLINE_JAVASCRIPT_REQUIREMENT_KEYWORD; }
        "inputBinding"                     { return CwlTokenTypes.INPUT_BINDING_KEYWORD; }
        "inputs"                           { return CwlTokenTypes.INPUTS_KEYWORD; }
        "int"                              { return CwlTokenTypes.INT_TYPE_KEYWORD; }
        "items"                            { return CwlTokenTypes.ITEMS_KEYWORD; }
        "itemSeparator"                    { return CwlTokenTypes.ITEM_SEPARATOR_KEYWORD; }
        "label"                            { return CwlTokenTypes.LABEL_KEYWORD; }
        "{"                                { return CwlTokenTypes.LBRACE; }
        "["                                { return CwlTokenTypes.LBRACKET; }
        "listing"                          { return CwlTokenTypes.LISTING_KEYWORD; }
        "linkMerge"                        { return CwlTokenTypes.LINK_MERGE_KEYWORD; }
        "loadContents"                     { return CwlTokenTypes.LOAD_CONTENTS_KEYWORD; }
        "location"                         { return CwlTokenTypes.LOCATION_KEYWORD; }
        "long"                             { return CwlTokenTypes.LONG_TYPE_KEYWORD; }
        "("                                { return CwlTokenTypes.LPARENTHESIS; }
        "merge_nested"                     { return CwlTokenTypes.MERGE_NESTED_KEYWORD; }
        "merge_flattened"                  { return CwlTokenTypes.MERGE_FLATTENED_KEYWORD; }
        "$mixin"                           { return CwlTokenTypes.MIXIN_KEYWORD; }
        "MultipleInputFeatureRequirement"  { return CwlTokenTypes.MULTIPLE_INPUT_FEATURE_REQUIREMENT_KEYWORD; }
        "name"                             { return CwlTokenTypes.NAME_KEYWORD; }
        "nameext"                          { return CwlTokenTypes.NAMEEXT_KEYWORD; }
        "nameroot"                         { return CwlTokenTypes.NAMEROOT_KEYWORD; }
        "nested_crossproduct"              { return CwlTokenTypes.NESTED_CROSS_PRODUCT_KEYWORD; }
        "null"                             { return CwlTokenTypes.NULL_TYPE_KEYWORD; }
        "#"                                { return CwlTokenTypes.OCTOTHORPE; }
        "out"                              { return CwlTokenTypes.OUT_KEYWORD; }
        "outdirMax"                        { return CwlTokenTypes.OUTDIR_MAX_KEYWORD; }
        "outdirMin"                        { return CwlTokenTypes.OUTDIR_MIN_KEYWORD; }
        "outputEval"                       { return CwlTokenTypes.OUTPUT_EVAL_KEYWORD; }
        "outputBinding"                    { return CwlTokenTypes.OUTPUT_BINDING_KEYWORD; }
        "outputs"                          { return CwlTokenTypes.OUTPUTS_KEYWORD; }
        "outputSource"                     { return CwlTokenTypes.OUTPUT_SOURCE_KEYWORD; }
        "package"                          { return CwlTokenTypes.PACKAGE_KEYWORD; }
        "packages"                         { return CwlTokenTypes.PACKAGES_KEYWORD; }
        "path"                             { return CwlTokenTypes.PATH_KEYWORD; }
        "permanentFailCodes"               { return CwlTokenTypes.PERMANENT_FAIL_CODES_KEYWORD; }
        "position"                         { return CwlTokenTypes.POSITION_KEYWORD; }
        "prefix"                           { return CwlTokenTypes.PREFIX_KEYWORD; }
        "ramMax"                           { return CwlTokenTypes.RAM_MAX_KEYWORD; }
        "ramMin"                           { return CwlTokenTypes.RAM_MIN_KEYWORD; }
        "run"                              { return CwlTokenTypes.RUN_KEYWORD; }
        "}"                                { return CwlTokenTypes.RBRACE; }
        "]"                                { return CwlTokenTypes.RBRACKET; }
        "record"                           { return CwlTokenTypes.RECORD_KEYWORD; }
        "requirements"                     { return CwlTokenTypes.REQUIREMENTS_KEYWORD; }
        "ResourceRequirement"              { return CwlTokenTypes.RESOURCE_REQUIREMENT_KEYWORD; }
        ")"                                { return CwlTokenTypes.RPARENTHESIS; }
        "scatter"                          { return CwlTokenTypes.SCATTER_KEYWORD; }
        "ScatterFeatureRequirement"        { return CwlTokenTypes.SCATTER_FEATURE_REQUIREMENT_KEYWORD; }
        "scatterMethod"                    { return CwlTokenTypes.SCATTER_METHOD_KEYWORD; }
        "SchemaDefRequirement"             { return CwlTokenTypes.SCHEMA_DEF_REQUIREMENT_KEYWORD; }
        "secondaryFiles"                   { return CwlTokenTypes.SECONDARY_FILES_KEYWORD; }
        "separate"                         { return CwlTokenTypes.SEPARATE_KEYWORD; }
        "ShellCommandRequirement"          { return CwlTokenTypes.SHELL_COMMAND_REQUIREMENT_KEYWORD; }
        "shellQuote"                       { return CwlTokenTypes.SHELL_QUOTE_KEYWORD; }
        "size"                             { return CwlTokenTypes.SIZE_KEYWORD; }
        "SoftwareRequirement"              { return CwlTokenTypes.SOFTWARE_REQUIREMENT_KEYWORD; }
        "source"                           { return CwlTokenTypes.SOURCE_KEYWORD; }
        "specs"                            { return CwlTokenTypes.SPECS_KEYWORD; }
        "stderr"                           { return CwlTokenTypes.STDERR_KEYWORD; }
        "stdin"                            { return CwlTokenTypes.STDIN_KEYWORD; }
        "stdout"                           { return CwlTokenTypes.STDOUT_KEYWORD; }
        "StepInputExpressionRequirement"   { return CwlTokenTypes.STEP_INPUT_EXPRESSION_REQUIREMENT_KEYWORD; }
        "steps"                            { return CwlTokenTypes.STEPS_KEYWORD; }
        "streamable"                       { return CwlTokenTypes.STREAMABLE_KEYWORD; }
        "string"                           { return CwlTokenTypes.STRING_TYPE_KEYWORD; }
        "SubworkflowFeatureRequirement"    { return CwlTokenTypes.SUBWORKFLOW_FEATURE_REQUIREMENT_KEYWORD; }
        "successCodes"                     { return CwlTokenTypes.SUCCESS_CODES_KEYWORD; }
        "symbols"                          { return CwlTokenTypes.SYMBOLS_KEYWORD; }
        "temporaryFailCodes"               { return CwlTokenTypes.TEMPORARY_FAIL_CODES_KEYWORD; }
        "tmpdirMax"                        { return CwlTokenTypes.TMPDIR_MAX_KEYWORD; }
        "tmpdirMin"                        { return CwlTokenTypes.TMPDIR_MIN_KEYWORD; }
        "type"                             { return CwlTokenTypes.TYPE_KEYWORD; }
        "types"                            { return CwlTokenTypes.TYPES_KEYWORD; }
        "valueFrom"                        { return CwlTokenTypes.VALUE_FROM_KEYWORD; }
        "version"                          { return CwlTokenTypes.VERSION_KEYWORD; }
        "Workflow"                         { return CwlTokenTypes.WORKFLOW_KEYWORD; }
        "writable"                         { return CwlTokenTypes.WRITABLE_KEYWORD; }
        {String}                           { return CwlTokenTypes.STRING; }
    }

    <MULTILINE_STRING>{
        [\ ]                               { return CwlTokenTypes.SPACE; }
        [\t]                               { return CwlTokenTypes.TAB; }
        [\f]                               { return CwlTokenTypes.FORMFEED; }
        {LineTerminator}                   { yybegin(YYINITIAL);
                                             return CwlTokenTypes.LINE_BREAK; }
        // TODO allow spaces inside
        \S+                                { return CwlTokenTypes.MLSPART; }
    }

    .                                      { return TokenType.BAD_CHARACTER;}