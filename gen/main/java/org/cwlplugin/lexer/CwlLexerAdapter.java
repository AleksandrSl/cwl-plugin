package org.cwlplugin.lexer;

import com.intellij.lexer.FlexAdapter;

public class CwlLexerAdapter extends FlexAdapter {

    private CwlFlexLexer myFlex;

    public CwlLexerAdapter() {
        super(new _CwlLexer());
        myFlex = (_CwlLexer) super.getFlex();
    }

    public CwlFlexLexer getFlex() {
        return myFlex;
    }
}
