cwlVersion: v1.0
class: CommandLineTool

baseCommand: [echo, -n]

label: "Example"

requirements:
    - class: DockerRequirement
      dockerFile: |
         some
          text
             describing
         image
    - class: InlineJavascriptRequirement
      expressionLib: [asd, asd]
    - class: SoftwareRequirement
      packages:
        - package: one
        - package: two
    - class: SoftwareRequirement
      packages:
         trimm: [asd, jjj]
    - class: SoftwareRequirement
      packages:
        trimmer:
          version: ["3.0"]
          specs: [text]
    - class: EnvVarRequirement
      envDef:
        - envName: "name"  # Keywords must be in parenthesis
          envValue: 'asdasd' # Single parenthesis will do also
        - envValue: value
    - class: EnvVarRequirement
      envDef:
        one: two
    - class: EnvVarRequirement
      envDef:
       one:
          envValue: two
          envName: three
    - class: SchemaDefRequirement
      types:
         - type: enum
           symbols: [one, two]
           inputBinding:
             position: 12312
             prefix: some prefix
           label: |
             After this pipe
                             symbols can
               follow any text
              untils it's
                                   indented
         - type: record
           label: rec
           fields:
            - name: forward_reads
              type: string
              inputBinding:
                position: 4
                prefix: -f
            - name: blank
         - type: array
           items: boolean
           label: boolean array
         - type: enum
           symbols:
            - one
            - two
           inputBinding:
             position: 123
         - type: record
           label: some record

    - class: ResourceRequirement
      coresMin: 4444
      coresMax: 23123123
      ramMin: string will do
      ramMax:  string will do
      tmpdirMin: $(and expression too)
      tmpdirMax: 123123
      outdirMin: 123
      outdirMax: 40232
    - class: InitialWorkDirRequirement
      listing:
        - "string"
        - str
        - $(expression)
        - class: File
          location: Shadowmoon Valley/Black Temple
          size: 13
          checksum: infinite
          secondaryFiles:
            - class: File
              location: Tanaris
              path: asdasd
              size: 123123
            - class: File
              location: Nagrand
              path: Haala
              size: 123123
            - class: Directory
              location: Teldrassil
              basename: Aldrassil
              listing:
                - class: File
                  location: https://www.nowhere.com
                  size: 123123
                - class: Directory
                  location: asasdasd
                  basename: asdasd
                  listing:
                    # And so on

                                - class: File
                                  location: asdasd
                                  path: asdasd
                                  size: 123123
                                - class: File
                                  location: asdasd
                                  path: asdasd
                                  size: 123123
                                - class: File
                                  location: asdasd
                                  path: asdasd
                                  size: 123123
                                - class: Directory
                                  location: asasdasd
                                  basename: asdasd
                                  listing:
                                                - class: File
                                                  location: asdasd
                                                  path: asdasd
                                                  size: 123123
                                                - class: File
                                                  location: asdasd
                                                  path: asdasd
                                                  size: 123123
                                                - class: File
                                                  location: asdasd
                                                  path: asdasd
                                                  size: 123123
                                                - class: Directory
                                                  location: asasdasd
                                                  basename: asdasd
                                                  listing:
                                                                - class: File
                                                                  location: asdasd
                                                                  path: asdasd
                                                                  size: 123123
                                                                - class: File
                                                                  location: asdasd
                                                                  path: asdasd
                                                                  size: 123123
                                                                - class: File
                                                                  location: asdasd
                                                                  path: asdasd
                                                                  size: 123123
        - entry: asasdasd
          entryname: $(asdasd)

temporaryFailCodes: [ 1, 3, 2]

arguments:
  - string_argument
  - position: 3
    prefix: "-f"
    valueFrom: second_argument
  - position: 5
    valueFrom: $()

inputs:
  first_parameter:
    type: boolean
    default: any type here
    streamable: True
    format: [some, list]
    doc:
     - asdasd
    inputBinding:
      position: 0
      prefix: "-123"
      itemSeparator: asd

  second_parameter:
    type: File
    secondaryFiles:
      - .tmap.anno
      - .tmap.bwt
      - .tmap.pac
      - .tmap.sa
      - .fai

outputs:
  out_f:
    type: boolean