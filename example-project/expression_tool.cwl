cwlVersion: v1.0
class: ExpressionTool
doc: |
  Return array of BAM files in directory

requirements:
  - class: InlineJavascriptRequirement

inputs:
  input_dir: Directory

outputs:
  bam_files: File[]

expression: |
  ${
    var samples = [];
    for (var i = 0; i < inputs.input_dir.listing.length; i++)
    {
      var file = inputs.input_dir.listing[i];
        if (file.basename.split(".").slice(-1)[0] == "bam")
          {
            samples.push(file);
          }
    }
    return {"bam_files": samples};
  }
