cwlVersion: v1.0
class: Workflow

requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: DockerRequirement
    dockerFile: |
        some
         text
            describing
        image

inputs:   # Two ways of input parameters declaration
  - type: string
    id: asdasd
  - type: int

inputs:
  input_reads_dir: Directory

outputs:
  genome_bam:
    type: asdasd
    format: asdasd
    outputSource: singlesample_pipeline/indexed_sorted_aligned_to_genome_reads
  hqr_bed:
    type: File[]
    outputSource: singlesample_pipeline/merged_hqr_bed
  lqr_bed:
    type: File[]
    outputSource: singlesample_pipeline/merged_lqr_bed
  small_variants:
    type: File[]
    outputSource: singlesample_pipeline/small_variants
    linkMerge: merge_nested


steps:
  step_1:
    run: expression_tool.cwl
    in:
      input_dir: input_reads_dir
    out: [files]

  step_2:
    run: subfolder/command_line_tool.cwl
    in:
      first_parameter: get_input_reads/bam_files
      second_parameter: reference_genome
    out: [first output, second output, fourth output]